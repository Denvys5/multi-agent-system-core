package com.denvys5.agent;

import com.denvys5.multi_agent_system.controller.IntellectualAgentController;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;

import java.util.concurrent.ExecutionException;

public class AgentApp {
    public static void main(String[] args) {
        String agentEnumeration = AgentEnumeration.MANAGEMENT_ACS_AGENT.toString();

        IntellectualAgentController intellectualAgentController = new IntellectualAgentController(agentEnumeration);
        intellectualAgentController.setRelativeClass(AgentApp.class);
        intellectualAgentController.setDevMode(true);

        intellectualAgentController.start();
        try {
            intellectualAgentController.untilReadyBlock();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }
}
