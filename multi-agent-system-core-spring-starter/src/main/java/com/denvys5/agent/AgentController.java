package com.denvys5.agent;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.controller.*;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.service.AgentRequestIdTrackingService;
import com.denvys5.multi_agent_system.service.IShutdownService;
import com.denvys5.multi_agent_system.service.event.AgentEventService;
import com.denvys5.multi_agent_system.service.message_io.*;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

@Service
public class AgentController {
    private final IntellectualAgentController intellectualAgentController;

    public AgentController(ShutdownService shutdownService, String agentEnumeration) {

        intellectualAgentController = new IntellectualAgentController(AgentEnumeration.valueOf(agentEnumeration));
        intellectualAgentController.setDevMode(true);
        intellectualAgentController.setRelativeClass(this.getClass());
        intellectualAgentController.setShutdownService(shutdownService);
    }

    @Bean
    public IntellectualAgentController getIntellectualAgentController() {
        return intellectualAgentController;
    }

    @Bean
    public IMultiAgentSystemConfiguration getConfigController() {
        return intellectualAgentController.getConfigController();
    }

    @Bean
    public CommunicationController getCommunicationController() {
        return intellectualAgentController.getCommunicationController();
    }

    @Bean
    public MultiAgentNetworkController getMultiAgentNetworkController() {
        return intellectualAgentController.getMultiAgentNetworkController();
    }

    @Bean
    public AgentDiscoveryController getAgentDiscoveryController() {
        return intellectualAgentController.getAgentDiscoveryController();
    }

    @Bean
    public IMessageReceiveFactory getMessageReceiveFactory() {
        return intellectualAgentController.getMessageReceiveFactory();
    }

    @Bean
    public IMessageProcessor getAgentMessageProcessor() {
        return intellectualAgentController.getAgentMessageProcessor();
    }

    @Bean
    public MessageReceiveEventService getMessageReceiveEventService() {
        return intellectualAgentController.getMessageReceiveEventService();
    }

    @Bean
    public HandshakeFactory getHandshakeFactory() {
        return intellectualAgentController.getHandshakeFactory();
    }

    @Bean
    public IMessageFactory getAgentMessageFactory() {
        return intellectualAgentController.getAgentMessageFactory();
    }

    @Bean
    public AgentStatusController getAgentStatusController() {
        return intellectualAgentController.getAgentStatusController();
    }

    @Bean
    public AgentRequestIdTrackingService getAgentRequestIdTrackingService() {
        return intellectualAgentController.getAgentRequestIdTrackingService();
    }

    @Bean
    public AgentEventService getAgentEventService() {
        return intellectualAgentController.getAgentEventService();
    }

    @Bean
    public IMessageSerializationService getMessageSerializationService() {
        return intellectualAgentController.getMessageSerializationService();
    }

    @Bean
    public IShutdownService getShutdownService() {
        return intellectualAgentController.getShutdownService();
    }

    @Bean
    public ICryptoService getCryptoService() {
        return intellectualAgentController.getCryptoService();
    }

    @PreDestroy
    public void shutdown(){
        intellectualAgentController.shutdown();
    }
}
