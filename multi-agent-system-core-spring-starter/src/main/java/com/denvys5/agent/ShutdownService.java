package com.denvys5.agent;

import com.denvys5.multi_agent_system.service.IShutdownService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ShutdownService implements IShutdownService {
    @Setter
    private Runnable shutdownSequence;

    @Value("${sefl.properties.restartCommand}")
    private String restartCommand;

    @Value("${sefl.properties.shutdownCommand}")
    private String shutdownCommand;

    public void restartServer(){
        try {
            Runtime.getRuntime().exec(restartCommand);
        } catch (IOException ignored) {
        }
    }

    public void shutdownServer(){
        try {
            Runtime.getRuntime().exec(shutdownCommand);
        } catch (IOException ignored) {
        }
    }

    @Override
    public void shutdownByForce() {
        shutdownServer();
    }

    @Override
    public void shutdownServices() {
        shutdownSequence.run();
    }

    @Override
    public void restartByForce() {
        restartServer();
    }
}
