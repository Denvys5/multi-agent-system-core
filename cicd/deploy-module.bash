#!/bin/bash

mvn $MAVEN_CLI_OPTS -DskipTests deploy -N --fail-never
mvn $MAVEN_CLI_OPTS -DskipTests deploy -pl $1
