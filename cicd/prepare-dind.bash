#!/bin/bash

echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
sed -i "s?$CI_REGISTRY?$CI_SERVER_PROTOCOL://$CI_SERVER_HOST:$CI_SERVER_PORT?" ~/.docker/config.json
