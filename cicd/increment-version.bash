#!/bin/bash

BRANCH_NAME=${CI_COMMIT_BRANCH}

git config --global user.email "project${CI_PROJECT_ID}_bot@noreply.${CI_SERVER_HOST}"
git config --global user.name "project${CI_PROJECT_ID}_bot"
git remote set-url origin "https://git-token:${GIT_PUSH_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
git tag -d $(git tag -l)
git fetch
git checkout ${BRANCH_NAME}
git reset --hard origin/${BRANCH_NAME}

export CURRENT_VERSION=$(mvn help:evaluate -Dexpression=revision -q -DforceStdout -N)
echo Released version: $CURRENT_VERSION

git tag -a "v${CURRENT_VERSION}" -m "Release version ${CURRENT_VERSION}"
git push -o ci.skip --tags origin "v${CURRENT_VERSION}"

export NEW_VERSION=$(awk -vFS=. -vOFS=. '{$NF++;print}' <<< $CURRENT_VERSION)
mvn versions:set-property -Dproperty=revision -DnewVersion=$NEW_VERSION versions:commit
echo Next version: $(mvn help:evaluate -Dexpression=revision -q -DforceStdout)
COMMIT_MESSAGE="[Increment version script] Generating new version: $NEW_VERSION"

git add pom.xml
git commit -m "${COMMIT_MESSAGE}"
git push -o ci.skip origin ${BRANCH_NAME}
