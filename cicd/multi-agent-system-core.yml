variables:
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2
    -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Djava.awt.headless=true
  MAVEN_CLI_OPTS: >-
    --batch-mode
    --errors
    --fail-at-end
    --show-version
    --no-transfer-progress
    -DinstallAtEnd=true
    -s ci_settings.xml
    -Dchangelist=$CHANGELIST_VAR

image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/maven:3.9-amazoncorretto-11-al2023
cache:
  paths:
    - .m2/repository

.ruleset:
  rules:
    - &ignore-merge-request
      if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never
    - &changelist-rule-anchor
      if: $CI_COMMIT_BRANCH != "master"
      variables:
        CHANGELIST_VAR: ".$CI_PIPELINE_IID-SNAPSHOT"
      when: on_success
    - &changelist-rule-develop-branch-anchor
      if: $CI_COMMIT_BRANCH == "develop"
      variables:
        CHANGELIST_VAR: ".$CI_PIPELINE_IID-SNAPSHOT"
      when: on_success
    - &changelist-rule-unprotected-manual-branch-anchor
      if: $CI_COMMIT_BRANCH != "master" && $CI_COMMIT_BRANCH != "develop"
      variables:
        CHANGELIST_VAR: ".$CI_PIPELINE_IID-SNAPSHOT"
      when: manual
    - &rule-master-branch-anchor
      if: $CI_COMMIT_BRANCH == "master"
      when: on_success


build-artifact-core:
  stage: build
  rules:
    - *ignore-merge-request
    - *changelist-rule-anchor
    - *rule-master-branch-anchor
  variables:
    MODULE_NAME: "multi-agent-system-core"
  script:
    - "/bin/bash cicd/build-parent.bash"
    - "/bin/bash cicd/build-module.bash $MODULE_NAME"
  artifacts:
    paths:
      - /builds/$CI_PROJECT_PATH/$MODULE_NAME/target/*.jar

test-core:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/denvys5/maven-docker:corretto-11-al2023
  rules:
    - *ignore-merge-request
    - *changelist-rule-anchor
    - *rule-master-branch-anchor
  variables:
    MODULE_NAME: "multi-agent-system-core"
  tags:
    - dind
  script:
    - "/bin/bash cicd/prepare-dind.bash"
    - "/bin/bash cicd/build-parent.bash"
    - "/bin/bash cicd/test-module.bash $MODULE_NAME"
  artifacts:
    when: always
    reports:
      junit:
        - $MODULE_NAME/target/surefire-reports/TEST-*.xml
        - $MODULE_NAME/target/failsafe-reports/TEST-*.xml
      coverage_report:
        coverage_format: jacoco
        path: $MODULE_NAME/target/site/jacoco/jacoco.xml

build-artifact-spring-starter:
  stage: build
  rules:
    - *ignore-merge-request
    - *changelist-rule-anchor
    - *rule-master-branch-anchor
  variables:
    MODULE_NAME: "multi-agent-system-core-spring-starter"
    DEPENDENCY_MODULE_NAME: "multi-agent-system-core"
  script:
    - "/bin/bash cicd/build-parent.bash"
    - "/bin/bash cicd/build-module.bash $DEPENDENCY_MODULE_NAME"
    - "/bin/bash cicd/build-module.bash $MODULE_NAME"
  artifacts:
    paths:
      - /builds/$CI_PROJECT_PATH/$MODULE_NAME/target/*.jar

deploy-artifact-private:
  stage: deploy
  rules:
    - *ignore-merge-request
    - *changelist-rule-develop-branch-anchor
    - *rule-master-branch-anchor
  variables:
    MODULE_NAME: "multi-agent-system-core"
  script:
    - "/bin/bash cicd/deploy-module-private.bash $MODULE_NAME"

deploy-artifact-central:
  stage: deploy
  rules:
    - *ignore-merge-request
    - *rule-master-branch-anchor
  variables:
    MODULE_NAME: "multi-agent-system-core"
  script:
    - "/bin/bash cicd/deploy-module-central.bash $MODULE_NAME"

increase-version:
  stage: deploy
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/denvys5/maven-git:corretto-11-al2023
  rules:
    - *ignore-merge-request
    - *rule-master-branch-anchor
  script:
    - "/bin/bash cicd/increment-version.bash"

