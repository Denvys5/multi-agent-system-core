package com.denvys5.multi_agent_system.model.service.network.mqtt;

import com.denvys5.multi_agent_system.util.ContainerUtils;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.*;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.hivemq.HiveMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.MountableFile;

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Log4j2
public class MqttSSLConnectorTest {
    private final int mqttPort = 8883;
    private final MqttConnectorCommonTest mqttConnectorCommonTest = new MqttConnectorCommonTest(log);

    @Container
    private final GenericContainer<?> container = new HiveMQContainer(ContainerUtils.getDockerImageNameCiFriendly("hivemq/hivemq-ce:2024.9"))
            .withExposedPorts(mqttPort)
            .withCopyFileToContainer(
                    MountableFile.forClasspathResource("mqtt/hivemq.jks"),
                    "/opt/hivemq/")
            .withHiveMQConfig(MountableFile.forClasspathResource("/mqtt/hivemqConfig.xml"));

    @Test
    @Tag("MqttSSL")
    @Tag("Testcontainers")
    public void publishTest() throws MqttException {
        mqttConnectorCommonTest.publishTest(container.getHost(), container.getMappedPort(mqttPort), true);
    }

    @Test
    @Tag("MqttSSL")
    @Tag("Testcontainers")
    public void subscribeTest() throws MqttException, InterruptedException {
        mqttConnectorCommonTest.subscribeTest(container.getHost(), container.getMappedPort(mqttPort), true);
    }
}
