package com.denvys5.multi_agent_system.model.service.network.mqtt;

import com.denvys5.multi_agent_system.util.ContainerUtils;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.*;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Log4j2
public class MqttConnectorTest {
    private final int mqttPort = 1883;
    private final MqttConnectorCommonTest mqttConnectorCommonTest = new MqttConnectorCommonTest(log);

    @Container
    private final GenericContainer<?> container = new GenericContainer<>(ContainerUtils.getDockerImageNameCiFriendly("hivemq/hivemq-ce:2024.9"))
            .withExposedPorts(mqttPort);

    @Test
    @Tag("Testcontainers")
    public void publishTest() throws MqttException {
        mqttConnectorCommonTest.publishTest(container.getHost(), container.getMappedPort(mqttPort), false);
    }

    @Test
    @Tag("Testcontainers")
    public void subscribeTest() throws MqttException, InterruptedException {
        mqttConnectorCommonTest.subscribeTest(container.getHost(), container.getMappedPort(mqttPort), false);
    }
}
