package com.denvys5.multi_agent_system.model.service.network.mqtt;

import com.denvys5.multi_agent_system.util.MqttUtils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RequiredArgsConstructor
public class MqttConnectorCommonTest {
    private final String topicOut = "srv/test";
    private final Logger log;

    public void publishTest(String hostname, int port, boolean sslEnabled) throws MqttException {
        IMqttClient client = MqttUtils.connect(hostname, port, sslEnabled, this.getClass());

        sendMessage(client, MqttUtils.getMessage());
    }

    public void sendMessage(IMqttClient client, String message) throws MqttException {
        MqttMessage msg = new MqttMessage(message.getBytes(StandardCharsets.UTF_8));
        msg.setQos(0);
        client.publish(topicOut, msg);
    }

    public void subscribeTest(String hostname, int port, boolean sslEnabled) throws MqttException, InterruptedException {
        IMqttClient client = MqttUtils.connect(hostname, port, sslEnabled, this.getClass());

        CountDownLatch receivedSignal = new CountDownLatch(1);
        String message = MqttUtils.getMessage();
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        client.subscribe(topicOut, (topic, msg) -> {
            byte[] payload = msg.getPayload();
            String string = new String(payload, StandardCharsets.UTF_8);
            log.info("Received: {}", string);
            completableFuture.complete(string);
            receivedSignal.countDown();
        });

        sendMessage(client, message);

        assertTrue(receivedSignal.await(2, TimeUnit.SECONDS));
        assertEquals(message, completableFuture.getNow(""));
    }

}
