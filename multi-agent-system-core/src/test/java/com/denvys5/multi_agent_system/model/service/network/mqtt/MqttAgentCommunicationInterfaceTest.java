package com.denvys5.multi_agent_system.model.service.network.mqtt;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.config.impl.FileMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.communication.MqttAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.model.enumeration.CommunicationMethodEnumeration;
import com.denvys5.multi_agent_system.service.message_io.MessageSerializationService;
import com.denvys5.multi_agent_system.service.network.mqtt.MqttAgentCommunicationInterface;
import com.denvys5.multi_agent_system.util.ContainerUtils;
import com.denvys5.multi_agent_system.util.MqttUtils;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Log4j2
@Tag("Testcontainers")
public class MqttAgentCommunicationInterfaceTest {
    private final int mqttPort = 1883;
    private final String topicOut = "/test/1";

    @Container
    private final GenericContainer<?> container = new GenericContainer<>(ContainerUtils.getDockerImageNameCiFriendly("hivemq/hivemq-ce:2024.9"))
            .withExposedPorts(mqttPort);

    private MqttAgentCommunicationInterface mqttAgentCommunicationInterface;

    @BeforeEach
    public void setUp() throws InterruptedException {
        IMultiAgentSystemConfiguration configController = new FileMultiAgentSystemConfiguration(null, true);
        configController.init();

        AgentCommunicationData agentCommunicationData = new AgentCommunicationData((byte) 1,
                CommunicationMethodEnumeration.MQTT_BROKER,
                new MqttAgentCommunicationInterfaceDataDto(container.getHost(),
                        container.getMappedPort(mqttPort),
                        false,
                        "admin",
                        "hivemq",
                        "acs/agent/topic1",
                        "acs/broadcast1")
        );

        mqttAgentCommunicationInterface = new MqttAgentCommunicationInterface(agentCommunicationData);
        mqttAgentCommunicationInterface.setConfigController(configController);
        mqttAgentCommunicationInterface.setMessageSerializationService(new MessageSerializationService());
        mqttAgentCommunicationInterface.init();
        mqttAgentCommunicationInterface.connect();

        Thread.sleep(500);
    }

    @AfterEach
    public void afterEach(){
        mqttAgentCommunicationInterface.disconnect();
    }

    @Test
    @Tag("Testcontainers")
    public void test() throws InterruptedException {
        String topic = "test-topic";

        CompletableFuture<String> completableFuture = new CompletableFuture<>();

        mqttAgentCommunicationInterface.subscribe(topic,
                (from, payload) -> {
            log.info("Payload received {}", payload);
            completableFuture.complete(payload);
        });
        mqttAgentCommunicationInterface.thenConnected(
                (client) -> log.info("Connected {}", client)
        );
        log.info("SUBSCRIBED");

        String payload = "testPayload";
        mqttAgentCommunicationInterface.publish(topic, payload);
        log.info("PUBLISHED");

        String response = null;
        try {
            response = completableFuture.get(2000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            log.error("Timed Out");
            Assertions.fail("Timed Out");
        }
        log.info("Response is: {}; expected: {}", response, payload);
        assertEquals(payload, response);
    }

    @Test
    @Tag("Testcontainers")
    public void publishTest() {
        sendMessage(mqttAgentCommunicationInterface, MqttUtils.getMessage());

        if (!mqttAgentCommunicationInterface.isConnected()) {
            Assertions.fail();
        }
    }

    public void sendMessage(MqttAgentCommunicationInterface mqttAgentCommunicationInterface, String message) {
        mqttAgentCommunicationInterface.publish(topicOut, message);
    }

    @Test
    @Tag("Testcontainers")
    public void subscribeTest() throws InterruptedException {
        CountDownLatch receivedSignal = new CountDownLatch(1);
        String message = MqttUtils.getMessage();
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        mqttAgentCommunicationInterface.subscribe(topicOut, (agentCommunicationData, msg) -> {
            log.info("Received: {}", msg);
            completableFuture.complete(msg);
            receivedSignal.countDown();
        });

        sendMessage(mqttAgentCommunicationInterface, message);

        assertTrue(receivedSignal.await(2, TimeUnit.SECONDS));
        assertEquals(message, completableFuture.getNow(""));
    }
}
