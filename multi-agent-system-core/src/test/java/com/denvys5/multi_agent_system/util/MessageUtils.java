package com.denvys5.multi_agent_system.util;

import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.message_io.IMessageFactory;
import org.junit.jupiter.api.Assertions;

import java.util.Collection;

public class MessageUtils {

    public static void assertMessageEqual(BasicAgentMessageDto expectedMessage, BasicAgentMessage actualMessage) {
        Assertions.assertEquals(expectedMessage.getToAgent(), actualMessage.getToAgent());
        Assertions.assertEquals(expectedMessage.getFromAgent(), actualMessage.getFromAgent());
        Assertions.assertEquals(expectedMessage.getBroadcastId(), actualMessage.getBroadcastId().getTypeValue());
        Assertions.assertEquals(expectedMessage.getCommandID(), actualMessage.getCommandID().getCommandValue());
        Assertions.assertEquals(expectedMessage.getDataCommandID(), actualMessage.getDataCommandID());
        Assertions.assertEquals(expectedMessage.getDataVal(), actualMessage.getDataVal());
        Assertions.assertEquals(expectedMessage.getData(), actualMessage.getData());
    }

    public static void assertMessageEqual(BasicAgentMessageDto expectedMessage, String expectedData, BasicAgentMessage actualMessage, String actualData) {
        Assertions.assertEquals(expectedMessage.getToAgent(), actualMessage.getToAgent());
        Assertions.assertEquals(expectedMessage.getFromAgent(), actualMessage.getFromAgent());
        Assertions.assertEquals(expectedMessage.getBroadcastId(), actualMessage.getBroadcastId().getTypeValue());
        Assertions.assertEquals(expectedMessage.getCommandID(), actualMessage.getCommandID().getCommandValue());
        Assertions.assertEquals(expectedMessage.getDataCommandID(), actualMessage.getDataCommandID());
        Assertions.assertEquals(expectedMessage.getDataVal(), actualMessage.getDataVal());
        Assertions.assertEquals(expectedData, actualData);
    }

    public static BasicAgentMessageDto getMessageDto(IMessageFactory messageFactory, int iterator, String data){
        return messageFactory.getDataMessage(iterator, (short) 1, (short) 0, data);
    }

    public static BasicAgentMessageDto getMessageDto(IMessageFactory messageFactory, IntellectualAgent toAgent, int iterator, String data){
        return messageFactory.getSecuredDataMessage(iterator, (short) 1, (short) 0, toAgent, data);
    }

    public static void awaitMessagesTwoStreams(int targetSize, Collection<?> collection1, Collection<?> collection2) throws InterruptedException {
        int maxTimeout = 30000;
        long start = System.currentTimeMillis();
        long maxTimestamp = start + maxTimeout;
        long timestamp;
        int sizeDiff1 = 0;
        int sizeDiff2 = 0;
        while((timestamp = System.currentTimeMillis()) < maxTimestamp &&
                (sizeDiff1 < collection1.size() ||
                sizeDiff2 < collection2.size())){
            sizeDiff1 = collection1.size();
            sizeDiff2 = collection2.size();
            Thread.sleep(250);
        }
        Assertions.assertTrue(timestamp < maxTimestamp);
        Assertions.assertEquals(targetSize, collection1.size());
        Assertions.assertEquals(targetSize, collection2.size());
    }

    public static void awaitMessagesOneStream(int targetSize, Collection<?> collection) throws InterruptedException {
        int maxTimeout = 30000;
        long start = System.currentTimeMillis();
        long maxTimestamp = start + maxTimeout;
        long timestamp;
        int sizeDiff2 = 0;
        while((timestamp = System.currentTimeMillis()) < maxTimestamp &&
                sizeDiff2 < collection.size()){
            sizeDiff2 = collection.size();
            Thread.sleep(250);
        }
        Assertions.assertTrue(timestamp < maxTimestamp);
        Assertions.assertEquals(targetSize, collection.size());
    }
}
