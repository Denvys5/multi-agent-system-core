package com.denvys5.multi_agent_system.util;

import org.apache.commons.lang3.StringUtils;
import org.testcontainers.utility.DockerImageName;

public class ContainerUtils {

    public static String getContainerStringCiFriendly(String container){
        String ciDependencyProxyGroupImagePrefix = System.getenv("CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX");

        return StringUtils.isNotEmpty(ciDependencyProxyGroupImagePrefix) ?
                ciDependencyProxyGroupImagePrefix + "/" + container :
                container;
    }

    public static DockerImageName getDockerImageNameCiFriendly(String container){
        String containerStringCiFriendly = getContainerStringCiFriendly(container);

        return containerStringCiFriendly.equals(container) ?
                DockerImageName.parse(container) :
                DockerImageName.parse(containerStringCiFriendly).asCompatibleSubstituteFor(container);
    }
}
