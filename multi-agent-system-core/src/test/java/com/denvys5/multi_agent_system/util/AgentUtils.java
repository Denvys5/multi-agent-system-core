package com.denvys5.multi_agent_system.util;

import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.denvys5.multi_agent_system.config.dto.communication.MqttAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.impl.InMemoryCommunicationConfiguration;
import com.denvys5.multi_agent_system.config.impl.InMemoryMultiAgentNetworkConfiguration;
import com.denvys5.multi_agent_system.config.impl.InMemoryMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.controller.IntellectualAgentController;
import com.denvys5.multi_agent_system.controller.MultiAgentNetworkController;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.agent.dto.AgentCommunicationDataDto;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommunicationMethodEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.message_io.SimpleDataMessageProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class AgentUtils {
    private static final Logger log = LogManager.getLogger(AgentUtils.class);

    public static IntellectualAgentController prepareAgent(String alias, Consumer<BasicAgentMessage> consumer, String containerHost, int containerPort){
        IntellectualAgentController agentController = new IntellectualAgentController(AgentEnumeration.DEFAULT);

        InMemoryMultiAgentSystemConfiguration multiAgentSystemConfiguration = new InMemoryMultiAgentSystemConfiguration();
        multiAgentSystemConfiguration.setDebug(true);
        multiAgentSystemConfiguration.setVerboseCommunication(true);
        multiAgentSystemConfiguration.setAgentNameAlias(alias);
        multiAgentSystemConfiguration.setAgentUUID("uuid-" + alias);
        multiAgentSystemConfiguration.setMinimalInitialTimeout(1000);
        multiAgentSystemConfiguration.setReconnectDelay(1000);
        multiAgentSystemConfiguration.setSecureCommunication(false);
        multiAgentSystemConfiguration.setMinimalInitialTimeout(10000);
        multiAgentSystemConfiguration.setPingTimeout(10000);
        multiAgentSystemConfiguration.setMaximumTimeout(25000);
        agentController.setConfigController(multiAgentSystemConfiguration);

        InMemoryCommunicationConfiguration communicationConfiguration = new InMemoryCommunicationConfiguration();
        AgentCommunicationDataDto agentCommunicationDataDto = new AgentCommunicationDataDto((byte) 1,
                CommunicationMethodEnumeration.MQTT_BROKER.getCommsId(),
                new MqttAgentCommunicationInterfaceDataDto(containerHost,
                        containerPort,
                        false,
                        "admin",
                        "hivemq",
                        "acs/" + alias.toLowerCase() + "/topic1",
                        "acs/broadcast1")
        );
        communicationConfiguration.setAgentCommunicationConfiguration(new AgentCommunicationConfiguration(Collections.singletonList(agentCommunicationDataDto)));
        agentController.setCommunicationConfiguration(communicationConfiguration);

        InMemoryMultiAgentNetworkConfiguration multiAgentNetworkConfiguration = new InMemoryMultiAgentNetworkConfiguration();
        agentController.setMultiAgentNetworkConfiguration(multiAgentNetworkConfiguration);

        agentController.setDataCommandProcessor(new SimpleDataMessageProcessor() {
            @Override
            public void processCommand(AgentCommunicationData readFrom, BasicAgentMessage message) {
                log.info("Consuming message in agent {} {}", alias, message);
                consumer.accept(message);
            }

            @Override
            public void processSecuredCommand(AgentCommunicationData readFrom, IntellectualAgent intellectualAgent, BasicAgentMessage message, String data) {
                throw new IllegalStateException("Unexpected to encounter secure message");
            }
        });

        return agentController;
    }

    public static IntellectualAgentController prepareAgent(String alias, BiConsumer<BasicAgentMessage, String> biConsumer, String containerHost, int containerPort){
        IntellectualAgentController agentController = new IntellectualAgentController(AgentEnumeration.DEFAULT);

        InMemoryMultiAgentSystemConfiguration multiAgentSystemConfiguration = new InMemoryMultiAgentSystemConfiguration();
        multiAgentSystemConfiguration.setDebug(true);
        multiAgentSystemConfiguration.setVerboseCommunication(true);
        multiAgentSystemConfiguration.setAgentNameAlias(alias);
        multiAgentSystemConfiguration.setAgentUUID("uuid-" + alias);
        multiAgentSystemConfiguration.setMinimalInitialTimeout(1000);
        multiAgentSystemConfiguration.setReconnectDelay(1000);
        multiAgentSystemConfiguration.setSecureCommunication(true);
        multiAgentSystemConfiguration.setMinimalInitialTimeout(10000);
        multiAgentSystemConfiguration.setPingTimeout(10000);
        multiAgentSystemConfiguration.setMaximumTimeout(25000);
        agentController.setConfigController(multiAgentSystemConfiguration);

        InMemoryCommunicationConfiguration communicationConfiguration = new InMemoryCommunicationConfiguration();
        AgentCommunicationDataDto agentCommunicationDataDto = new AgentCommunicationDataDto((byte) 1,
                CommunicationMethodEnumeration.MQTT_BROKER.getCommsId(),
                new MqttAgentCommunicationInterfaceDataDto(containerHost,
                        containerPort,
                        false,
                        "admin",
                        "hivemq",
                        "acs/" + alias.toLowerCase() + "/topic1",
                        "acs/broadcast1")
        );
        communicationConfiguration.setAgentCommunicationConfiguration(new AgentCommunicationConfiguration(Collections.singletonList(agentCommunicationDataDto)));
        agentController.setCommunicationConfiguration(communicationConfiguration);

        InMemoryMultiAgentNetworkConfiguration multiAgentNetworkConfiguration = new InMemoryMultiAgentNetworkConfiguration();
        agentController.setMultiAgentNetworkConfiguration(multiAgentNetworkConfiguration);

        agentController.setDataCommandProcessor(new SimpleDataMessageProcessor() {
            @Override
            public void processSecuredCommand(AgentCommunicationData readFrom, IntellectualAgent intellectualAgent, BasicAgentMessage message, String data) {
                log.info("Consuming secure message in agent {} {}", alias, message);
                biConsumer.accept(message, data);
            }

            @Override
            public void processCommand(AgentCommunicationData readFrom, BasicAgentMessage message) {
                throw new IllegalStateException("Unexpected to encounter insecure message");
            }
        });

        return agentController;
    }

    public static IntellectualAgent getAgentFromNetwork(MultiAgentNetworkController multiAgentNetworkController){
        return multiAgentNetworkController
                .getMultiAgentNetwork()
                .getAgents()
                .stream()
                .findAny()
                .get();
    }

    public static void sendMessageToOtherAgent(MultiAgentNetworkController multiAgentNetworkController, IntellectualAgent agent, BasicAgentMessageDto dataMessage){
        multiAgentNetworkController.sendMessageToAgent(agent, dataMessage);
    }

    public static void awaitMultiAgentNetworkInitialization(MultiAgentNetworkController multiAgentNetworkController1, MultiAgentNetworkController multiAgentNetworkController2) throws InterruptedException {
        Supplier<List<IntellectualAgent>> supplier1 = () -> multiAgentNetworkController1.getMultiAgentNetwork().getAgents();
        Supplier<List<IntellectualAgent>> supplier2 = () -> multiAgentNetworkController2.getMultiAgentNetwork().getAgents();
        int maxTimeout = 30000;
        long start = System.currentTimeMillis();
        long maxTimestamp = start + maxTimeout;
        long timestamp;
        do {
            Thread.sleep(250);
        }while((timestamp = System.currentTimeMillis()) < maxTimestamp &&
                (supplier1.get().stream().noneMatch(IntellectualAgent::isOnline) ||
                        supplier2.get().stream().noneMatch(IntellectualAgent::isOnline)));
        Assertions.assertFalse(supplier1.get().isEmpty());
        Assertions.assertFalse(supplier2.get().isEmpty());
        Assertions.assertTrue(supplier1.get().stream().allMatch(IntellectualAgent::isOnline));
        Assertions.assertTrue(supplier2.get().stream().allMatch(IntellectualAgent::isOnline));
        Assertions.assertTrue(timestamp < maxTimestamp);
    }
}
