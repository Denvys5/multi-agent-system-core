package com.denvys5.multi_agent_system.util;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.jupiter.api.Assertions;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Random;
import java.util.UUID;

public class MqttUtils {

    public static SocketFactory getTruststoreFactory(String resourceName, Class<?> referenceClass) throws Exception {
        KeyStore trustStore = KeyStore.getInstance("JKS");

        ClassLoader classLoader = referenceClass.getClassLoader();
        try(InputStream in = new FileInputStream(classLoader.getResource(resourceName).getPath())){
            trustStore.load(in, "changeme".toCharArray());
        }catch (Exception e){
            throw new IOException("Unable to read resource " + resourceName);
        };

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);

        SSLContext sslCtx = SSLContext.getInstance("TLSv1.2");
        sslCtx.init(null, tmf.getTrustManagers(), null);
        return sslCtx.getSocketFactory();
    }

    public static IMqttClient connect(String hostname, int port, boolean sslEnabled, Class<?> referenceClass) {
        String publisherId = UUID.randomUUID().toString();
        String login = "admin";
        String password = "hivemq";
        String protocol = sslEnabled ? "ssl" : "tcp";

        IMqttClient client;

        try {
            client = new MqttClient(protocol + "://" + hostname + ":" + port, publisherId, new MemoryPersistence());

            MqttConnectOptions options = new MqttConnectOptions();

            if(sslEnabled){
                try {
                    options.setSocketFactory(MqttUtils.getTruststoreFactory("mqtt/mqtt-client-trust-store.jks", referenceClass));
                } catch (Exception e) {
                    LoggerFactory.getLogger(referenceClass).error("Error while setting up TLS", e);
                }
            }

            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(5);
            options.setUserName(login);
            options.setPassword(password.toCharArray());
            client.connect(options);
        } catch (Exception e) {
            throw new RuntimeException("Fail to connect to MQTT", e);
        }

        int maxTimeout = 20000;
        long start = System.currentTimeMillis();
        long maxTimestamp = start + maxTimeout;
        long timestamp;
        while((timestamp = System.currentTimeMillis()) < maxTimestamp &&
                !client.isConnected()){
            try {
                Thread.sleep(250);
            } catch (InterruptedException ignored) {
            }
        }
        Assertions.assertTrue(timestamp < maxTimestamp);
        Assertions.assertTrue(client.isConnected());

        return client;
    }

    public static String getMessage() {
        double temp =  80 + new Random().nextDouble() * 20.0;
        return String.format("T:%04.2f", temp);
    }
}
