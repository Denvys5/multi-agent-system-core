package com.denvys5.multi_agent_system.controller;

import com.denvys5.config.ConfigFactory;
import com.denvys5.crypto.CryptoService;
import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.model.agent.*;
import com.denvys5.multi_agent_system.model.agent.dto.AgentCommunicationDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommunicationMethodEnumeration;
import com.denvys5.multi_agent_system.config.dto.communication.MqttAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.denvys5.multi_agent_system.service.message_io.MessageSerializationService;
import com.denvys5.multi_agent_system.service.message_io.MultiAgentSystemConvertUtils;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ConfigControllerTest {

    @Test
    public void generateMultiAgentNetworkConfig() throws FileNotFoundException {
        AgentCommunicationConfiguration agentCommunicationConfiguration = new AgentCommunicationConfiguration(
                Arrays.asList(
                        new AgentCommunicationDataDto(
                                (byte) 1, CommunicationMethodEnumeration.MQTT_BROKER.getCommsId(),
                                new MqttAgentCommunicationInterfaceDataDto(
                                        "localhost",
                                        1883,
                                        false,
                                        "admin",
                                        "hivemq",
                                        "acs/agent/topic1",
                                        "acs/broadcast1")
                        )
                )
        );

        ConfigFactory.saveJsonObjectWithApproximateRelativeFilePath("communication.json", agentCommunicationConfiguration);

        AgentCommunicationConfiguration compare = ConfigFactory.getJsonObjectWithApproximateRelativeFilePath("communication.json", AgentCommunicationConfiguration.class);

        MultiAgentSystemConvertUtils multiAgentSystemConvertUtils = new MultiAgentSystemConvertUtils();
        multiAgentSystemConvertUtils.setMessageSerializationService(new MessageSerializationService());

        assertEquals(multiAgentSystemConvertUtils.getAgentCommunicationDataListFromDtos(agentCommunicationConfiguration), multiAgentSystemConvertUtils.getAgentCommunicationDataListFromDtos(compare));
    }

    @Test
    public void saveMultiAgentNetworkConfiguration(){
        MultiAgentNetworkConfiguration multiAgentNetworkConfiguration = new MultiAgentNetworkConfiguration(new ArrayList<>());

        ICryptoService cryptoService = new CryptoService();
        cryptoService.generateKeyPair();

        IntellectualAgent intellectualAgent = new IntellectualAgent();
        intellectualAgent.setAgentUuid("uuid");
        intellectualAgent.setAliasName("alias");
        intellectualAgent.setAgentEnumeration(AgentEnumeration.ACCESS_CONTROL_AGENT);
        intellectualAgent.setCommsMethods(Arrays.asList(new AgentCommunicationData((byte) 1, CommunicationMethodEnumeration.NONE, new AgentCommunicationInterfaceDataDto())));
        intellectualAgent.setPublicEcdhKeyB64(cryptoService.getPublicEcdhKeyB64());
        intellectualAgent.setPublicEcdsaKeyB64(cryptoService.getPublicEcdsaKeyB64());

        multiAgentNetworkConfiguration.getAgents().add(intellectualAgent);

        ConfigFactory.saveJsonObjectWithApproximateRelativeFilePath("multi_agent_network.json", multiAgentNetworkConfiguration.toDto());

    }
}
