package com.denvys5.multi_agent_system.controller;

import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.concurrent.ExecutionException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IntellectualAgentTest {
    private IntellectualAgentController intellectualAgentController;

    @BeforeAll
    public void setup(){
        intellectualAgentController = new IntellectualAgentController(AgentEnumeration.MANAGEMENT_ACS_AGENT);

        intellectualAgentController.start();
        try {
            intellectualAgentController.untilReadyBlock();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    @Test
    @Disabled
    public void test() throws InterruptedException {
        Thread.sleep(600000);
    }

}
