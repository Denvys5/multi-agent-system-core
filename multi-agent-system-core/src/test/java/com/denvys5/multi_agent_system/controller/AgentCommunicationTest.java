package com.denvys5.multi_agent_system.controller;

import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.util.AgentUtils;
import com.denvys5.multi_agent_system.util.ContainerUtils;
import com.denvys5.multi_agent_system.util.MessageUtils;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Log4j2
public class AgentCommunicationTest {
    private final int mqttPort = 1883;
    private IntellectualAgentController agentController1;
    private IntellectualAgentController agentController2;
    private final List<BasicAgentMessage> receiverList1 = new ArrayList<>();
    private final List<BasicAgentMessage> receiverList2 = new ArrayList<>();

    @Container
    private final GenericContainer<?> container = new GenericContainer<>(ContainerUtils.getDockerImageNameCiFriendly("hivemq/hivemq-ce:2024.9"))
            .withExposedPorts(mqttPort);

    @BeforeEach
    public void setup() throws ExecutionException, InterruptedException {
        agentController1 = AgentUtils.prepareAgent("Agent1", (message) -> receiverList1.add(message), container.getHost(), container.getMappedPort(mqttPort));
        agentController1.start();
        agentController2 = AgentUtils.prepareAgent("Agent2", (message) -> receiverList2.add(message), container.getHost(), container.getMappedPort(mqttPort));
        agentController2.start();
        agentController1.untilReadyBlock();
        agentController2.untilReadyBlock();

        AgentUtils.awaitMultiAgentNetworkInitialization(agentController1.getMultiAgentNetworkController(), agentController2.getMultiAgentNetworkController());
        Thread.sleep(2000);
    }

    @AfterEach
    public void rampDown() throws InterruptedException {
        receiverList1.clear();
        receiverList2.clear();
        agentController1.shutdown();
        agentController2.shutdown();
        Thread.sleep(1000);
    }

    @Test
    public void testMessagesParallelInterleaving() throws InterruptedException {
        int targetSize = 10;
        List<BasicAgentMessageDto> sentMessageDtos1 = new ArrayList<>();
        List<BasicAgentMessageDto> sentMessageDtos2 = new ArrayList<>();

        IntellectualAgent agentFromNetwork1 = AgentUtils.getAgentFromNetwork(agentController1.getMultiAgentNetworkController());
        IntellectualAgent agentFromNetwork2 = AgentUtils.getAgentFromNetwork(agentController2.getMultiAgentNetworkController());
        for (int i = 0; i < targetSize; i++) {
            String data = "Hello world " + i;
            BasicAgentMessageDto messageDto1 = MessageUtils.getMessageDto(agentController1.getAgentMessageFactory(), i, data);
            sentMessageDtos1.add(messageDto1);
            AgentUtils.sendMessageToOtherAgent(agentController1.getMultiAgentNetworkController(), agentFromNetwork1, messageDto1);

            BasicAgentMessageDto messageDto2 = MessageUtils.getMessageDto(agentController2.getAgentMessageFactory(), i, data);
            sentMessageDtos2.add(messageDto2);
            AgentUtils.sendMessageToOtherAgent(agentController2.getMultiAgentNetworkController(), agentFromNetwork2, messageDto2);
        }

        MessageUtils.awaitMessagesTwoStreams(targetSize, receiverList1, receiverList2);
        Assertions.assertEquals(targetSize, sentMessageDtos1.size());
        Assertions.assertEquals(targetSize, sentMessageDtos2.size());

        for (int i = 0; i < targetSize; i++) {
            MessageUtils.assertMessageEqual(sentMessageDtos1.get(i), receiverList2.get(i));
            MessageUtils.assertMessageEqual(sentMessageDtos2.get(i), receiverList1.get(i));
        }
    }

    @Test
    public void testMessagesSideSequential() throws InterruptedException {
        int targetSize = 10;
        List<BasicAgentMessageDto> sentMessageDtos1 = new ArrayList<>();
        List<BasicAgentMessageDto> sentMessageDtos2 = new ArrayList<>();

        IntellectualAgent agentFromNetwork1 = AgentUtils.getAgentFromNetwork(agentController1.getMultiAgentNetworkController());
        for (int i = 0; i < targetSize; i++) {
            String data = "Hello world " + i;
            BasicAgentMessageDto messageDto1 = MessageUtils.getMessageDto(agentController1.getAgentMessageFactory(), i, data);
            sentMessageDtos1.add(messageDto1);
            AgentUtils.sendMessageToOtherAgent(agentController1.getMultiAgentNetworkController(), agentFromNetwork1, messageDto1);
        }

        IntellectualAgent agentFromNetwork2 = AgentUtils.getAgentFromNetwork(agentController2.getMultiAgentNetworkController());
        for (int i = 0; i < targetSize; i++) {
            String data = "Hello world " + i;
            BasicAgentMessageDto messageDto2 = MessageUtils.getMessageDto(agentController2.getAgentMessageFactory(), i, data);
            sentMessageDtos2.add(messageDto2);
            AgentUtils.sendMessageToOtherAgent(agentController2.getMultiAgentNetworkController(), agentFromNetwork2, messageDto2);
        }

        MessageUtils.awaitMessagesTwoStreams(targetSize, receiverList1, receiverList2);
        Assertions.assertEquals(targetSize, sentMessageDtos1.size());
        Assertions.assertEquals(targetSize, sentMessageDtos2.size());

        for (int i = 0; i < targetSize; i++) {
            MessageUtils.assertMessageEqual(sentMessageDtos1.get(i), receiverList2.get(i));
            MessageUtils.assertMessageEqual(sentMessageDtos2.get(i), receiverList1.get(i));
        }
    }

    @Test
    public void testMessagesOneSide() throws InterruptedException {
        int targetSize = 10;
        List<BasicAgentMessageDto> sentMessageDtos1 = new ArrayList<>();

        IntellectualAgent agentFromNetwork1 = AgentUtils.getAgentFromNetwork(agentController1.getMultiAgentNetworkController());
        for (int i = 0; i < targetSize; i++) {
            String data = "Hello world " + i;
            BasicAgentMessageDto messageDto1 = MessageUtils.getMessageDto(agentController1.getAgentMessageFactory(), i, data);
            sentMessageDtos1.add(messageDto1);
            AgentUtils.sendMessageToOtherAgent(agentController1.getMultiAgentNetworkController(), agentFromNetwork1, messageDto1);
        }

        MessageUtils.awaitMessagesOneStream(targetSize, receiverList2);
        Assertions.assertEquals(targetSize, sentMessageDtos1.size());

        for (int i = 0; i < targetSize; i++) {
            MessageUtils.assertMessageEqual(sentMessageDtos1.get(i), receiverList2.get(i));
        }
    }
}
