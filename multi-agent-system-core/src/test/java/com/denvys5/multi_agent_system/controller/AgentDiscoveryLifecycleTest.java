package com.denvys5.multi_agent_system.controller;

import com.denvys5.multi_agent_system.config.impl.InMemoryMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.agent.dto.MultiAgentNetworkConfigurationDto;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.util.AgentUtils;
import com.denvys5.multi_agent_system.util.ContainerUtils;
import com.denvys5.multi_agent_system.util.MessageUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Log4j2
public class AgentDiscoveryLifecycleTest {
    private final int mqttPort = 1883;
    private IntellectualAgentController agentController1;
    private IntellectualAgentController agentController2;
    private final List<Pair<BasicAgentMessage, String>> receiverList1 = new ArrayList<>();
    private final List<Pair<BasicAgentMessage, String>> receiverList2 = new ArrayList<>();

    @Container
    private final GenericContainer<?> container = new GenericContainer<>(ContainerUtils.getDockerImageNameCiFriendly("hivemq/hivemq-ce:2024.9"))
            .withExposedPorts(mqttPort);

    @AfterEach
    public void rampDown() throws InterruptedException {
        receiverList1.clear();
        receiverList2.clear();
        agentController1.shutdown();
        agentController2.shutdown();
        Thread.sleep(1000);
    }

    @Test
    public void testStartupAndShutdownOneSide() throws InterruptedException, ExecutionException {
        agentController1 = initAgent("Agent1", receiverList1);
        Thread.sleep(3000);

        agentController2 = initAgent("Agent2", receiverList2);

        AgentUtils.awaitMultiAgentNetworkInitialization(agentController1.getMultiAgentNetworkController(), agentController2.getMultiAgentNetworkController());
        Thread.sleep(2000);

        messageExchange();

        receiverList1.clear();
        receiverList2.clear();
        agentController2.shutdown();
        Thread.sleep(2000);

        MultiAgentNetworkConfigurationDto multiAgentNetworkConfiguration2 = agentController2.getMultiAgentNetworkConfiguration().getMultiAgentNetworkConfiguration();

        agentController2 = initAgent("Agent2", multiAgentNetworkConfiguration2, receiverList2);

        AgentUtils.awaitMultiAgentNetworkInitialization(agentController1.getMultiAgentNetworkController(), agentController2.getMultiAgentNetworkController());
        Thread.sleep(2000);

        messageExchange();
    }

    @Test
    public void testStartupAndShutdownBothSides() throws InterruptedException, ExecutionException {
        agentController1 = initAgent("Agent1", receiverList1);
        Thread.sleep(3000);

        agentController2 = initAgent("Agent2", receiverList2);

        AgentUtils.awaitMultiAgentNetworkInitialization(agentController1.getMultiAgentNetworkController(), agentController2.getMultiAgentNetworkController());
        Thread.sleep(2000);

        messageExchange();

        receiverList1.clear();
        receiverList2.clear();
        agentController1.shutdown();
        agentController2.shutdown();
        Thread.sleep(2000);

        MultiAgentNetworkConfigurationDto multiAgentNetworkConfiguration1 = agentController1.getMultiAgentNetworkConfiguration().getMultiAgentNetworkConfiguration();
        MultiAgentNetworkConfigurationDto multiAgentNetworkConfiguration2 = agentController2.getMultiAgentNetworkConfiguration().getMultiAgentNetworkConfiguration();

        agentController1 = initAgent("Agent1", multiAgentNetworkConfiguration1, receiverList1);
        agentController2 = initAgent("Agent2", multiAgentNetworkConfiguration2, receiverList2);

        AgentUtils.awaitMultiAgentNetworkInitialization(agentController1.getMultiAgentNetworkController(), agentController2.getMultiAgentNetworkController());
        Thread.sleep(2000);

        messageExchange();
    }

    private IntellectualAgentController initAgent(String agentAlias, List<Pair<BasicAgentMessage, String>> receiverList) throws ExecutionException, InterruptedException {
        IntellectualAgentController agentController = AgentUtils.prepareAgent(agentAlias, (message, data) -> receiverList.add(Pair.of(message, data)), container.getHost(), container.getMappedPort(mqttPort));
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setMinimalInitialTimeout(1000);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setPingTimeout(1000);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setMaximumTimeout(2500);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setAgentDiscoveryTimeOut(3000);
        agentController.start();
        agentController.untilReadyBlock();

        return agentController;
    }

    private IntellectualAgentController initAgent(String agentAlias, MultiAgentNetworkConfigurationDto multiAgentNetworkConfigurationDto, List<Pair<BasicAgentMessage, String>> receiverList) throws ExecutionException, InterruptedException {
        IntellectualAgentController agentController = AgentUtils.prepareAgent(agentAlias, (message, data) -> receiverList.add(Pair.of(message, data)), container.getHost(), container.getMappedPort(mqttPort));
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setMinimalInitialTimeout(1000);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setPingTimeout(1000);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setMaximumTimeout(2500);
        ((InMemoryMultiAgentSystemConfiguration) agentController.getConfigController()).setAgentDiscoveryTimeOut(3000);
        agentController.getMultiAgentNetworkConfiguration().setMultiAgentNetworkConfiguration(multiAgentNetworkConfigurationDto);
        agentController.start();
        agentController.untilReadyBlock();

        return agentController;
    }

    private void messageExchange() throws InterruptedException {
        int targetSize = 1;
        List<Pair<BasicAgentMessageDto, String>> sentMessageDtos1 = new ArrayList<>();
        List<Pair<BasicAgentMessageDto, String>> sentMessageDtos2 = new ArrayList<>();

        IntellectualAgent agentFromNetwork1 = AgentUtils.getAgentFromNetwork(agentController1.getMultiAgentNetworkController());
        IntellectualAgent agentFromNetwork2 = AgentUtils.getAgentFromNetwork(agentController2.getMultiAgentNetworkController());
        for (int i = 0; i < targetSize; i++) {
            String data = "Hello world " + i;
            BasicAgentMessageDto messageDto1 = MessageUtils.getMessageDto(agentController1.getAgentMessageFactory(), agentFromNetwork1, i, data);
            sentMessageDtos1.add(Pair.of(messageDto1, data));
            AgentUtils.sendMessageToOtherAgent(agentController1.getMultiAgentNetworkController(), agentFromNetwork1, messageDto1);

            BasicAgentMessageDto messageDto2 = MessageUtils.getMessageDto(agentController2.getAgentMessageFactory(), agentFromNetwork2, i, data);
            sentMessageDtos2.add(Pair.of(messageDto2, data));
            AgentUtils.sendMessageToOtherAgent(agentController2.getMultiAgentNetworkController(), agentFromNetwork2, messageDto2);
        }

        MessageUtils.awaitMessagesTwoStreams(targetSize, receiverList1, receiverList2);
        Assertions.assertEquals(targetSize, sentMessageDtos1.size());
        Assertions.assertEquals(targetSize, sentMessageDtos2.size());

        for (int i = 0; i < targetSize; i++) {
            MessageUtils.assertMessageEqual(sentMessageDtos1.get(i).getLeft(), sentMessageDtos1.get(i).getRight(), receiverList2.get(i).getLeft(), receiverList2.get(i).getRight());
            MessageUtils.assertMessageEqual(sentMessageDtos2.get(i).getLeft(), sentMessageDtos2.get(i).getRight(), receiverList1.get(i).getLeft(), receiverList1.get(i).getRight());
        }
    }
}
