/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.model.enumeration;

import com.denvys5.multi_agent_system.model.enumeration.dto.AgentEnumerationDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum AgentEnumeration {
    DEFAULT("DEFAULT","Default",
            AgentTypeSetEnumeration.NONE),
    BROADCAST("BROADCAST", "Broadcast Reserved Type",
            AgentTypeSetEnumeration.EVERYONE),
    MANAGEMENT_ACS_AGENT("ManagementACS", "Management ACS Agent",
            AgentTypeSetEnumeration.MANAGEMENT_AGENT),
    ACCESS_CONTROL_AGENT("AccessControl", "Access Control Agent",
            AgentTypeSetEnumeration.ACCESS_CONTROL_AGENT),
    ;

    private static final Map<Byte, AgentEnumeration> agentTypeEnumToEnum = Arrays.stream(values()).collect(Collectors.toMap((k) -> k.agentTypeSet.getTypeValue(), (k) -> k));

    private final String agentType;
    private final String agentTypeNaming;
    private final AgentTypeSetEnumeration agentTypeSet;

    public AgentEnumerationDto toDto(){
        return new AgentEnumerationDto(agentType, agentTypeNaming, agentTypeSet.toDto());
    }

    public static AgentEnumeration getAgentEnumerationByTypeSetValue(byte value){
        return agentTypeEnumToEnum.getOrDefault(value, DEFAULT);
    }

    public static List<AgentEnumerationDto> getAsDtoList(){
        return Arrays.stream(values()).map(AgentEnumeration::toDto).collect(Collectors.toList());
    }
}
