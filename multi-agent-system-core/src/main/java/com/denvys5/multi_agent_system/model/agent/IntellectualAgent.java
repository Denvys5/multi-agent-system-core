/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.model.agent;

import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.crypto.SecretKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class IntellectualAgent {
    private String agentUuid;
    private String aliasName;
    private AgentEnumeration agentEnumeration;
    private StatusEntity status;
    private long lastMessageReceived;
    private List<AgentCommunicationData> commsMethods;
    private boolean secureCommunication = true; //default value for legacy compatibility
    private boolean online;
    private String publicEcdhKeyB64;
    private SecretKey secretEcdhKey;
    private String publicEcdsaKeyB64;
    private PublicKey publicEcdsaKey;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntellectualAgent agent = (IntellectualAgent) o;
        return agentUuid.equals(agent.agentUuid) && agentEnumeration == agent.agentEnumeration && secureCommunication == agent.secureCommunication && Objects.equals(commsMethods, agent.commsMethods) && Objects.equals(publicEcdsaKeyB64, agent.publicEcdsaKeyB64);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentUuid, agentEnumeration, secureCommunication, commsMethods, publicEcdsaKeyB64);
    }

    @Override
    public String toString() {
        return "IntellectualAgent{" +
                "agentUuid='" + agentUuid + '\'' +
                ", aliasName='" + aliasName + '\'' +
                ", agentEnumeration=" + agentEnumeration +
                ", status=" + status +
                ", lastMessageReceived=" + lastMessageReceived +
                ", commsMethods=" + commsMethods +
                ", secureCommunication=" + secureCommunication +
                ", online=" + online +
                '}';
    }
}
