/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.model.enumeration;

import com.denvys5.multi_agent_system.model.enumeration.dto.CommunicationMethodEnumerationDto;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public enum CommunicationMethodEnumeration {
    NONE(0, "NONE"),
    UDP_BROADCAST(1, "UDP Broadcast"),
    TCP_SOCKET(2, "TCP Socket"),
    HTTP_REST_API(3, "HTTP REST API"),
    MQTT_BROKER(4, "MQTT Broker"),
    KAFKA_BROKER(5, "KAFKA Broker"),
    ROS_BROKER(6, "ROS Broker"),
    ;
    private static final Map<Byte, CommunicationMethodEnumeration> byteToCommunicationMethodEnum = Arrays.stream(values()).collect(Collectors.toMap((k) -> k.commsId, (k) -> k));

    private final byte commsId;
    private final String commsNaming;

    CommunicationMethodEnumeration(int commsId, String commsNaming) {
        this.commsId = (byte) commsId;
        this.commsNaming = commsNaming;
    }

    public CommunicationMethodEnumerationDto toDto(){
        return new CommunicationMethodEnumerationDto(commsId, commsNaming);
    }

    public static CommunicationMethodEnumeration getCommsMethodFromId(byte id){
        return byteToCommunicationMethodEnum.getOrDefault(id, NONE);
    }
}
