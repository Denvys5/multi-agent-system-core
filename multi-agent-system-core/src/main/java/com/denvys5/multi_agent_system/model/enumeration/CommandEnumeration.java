/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.model.enumeration;

import com.denvys5.multi_agent_system.model.enumeration.dto.CommandEnumerationDto;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum CommandEnumeration {
    DEFAULT                             (0, false, false, 0, 0, "DEFAULT"),
    PING                                (1, false, false, -32767, 0, "Ping"),
    PING_RESPONSE                       (-32767, false, false, 0, 1, "Ping Response"),
    STATUS                              (2, false, false, -32766, 0, "Status"),
    STATUS_RESPONSE                     (-32766, false, true, 0, 2, "Status Response"),
    AGENT_DISCOVERY_BROADCAST           (10, true, false, 0, 0, "Agent Discovery Broadcast"),
    HANDSHAKE                           (11, true, false, -32757, 0, "Handshake"),
    HANDSHAKE_RESPONSE                  (-32757, false, true, 0, 11, "Handshake Response"),
    COMMUNICATION_INTERCHANGE           (14, false, false, -32754, 0, "Communication Interchange"),
    COMMUNICATION_INTERCHANGE_RESPONSE  (-32754, true, false, 0, 14, "Communication Interchange Response"),
    DATA_COMMAND                        (15, false, false, 0, 0, "Data Command"),
    ;
    private static final Map<Short, CommandEnumeration> shortToCommandEnum = Arrays.stream(values()).collect(Collectors.toMap((k) -> k.commandValue, (k) -> k));

    @Getter
    private final String commandNaming;
    @Getter
    private final short commandValue;
    private final boolean data;
    private final boolean dataval;
    @Getter
    private final short responseId;
    @Getter
    private final short responseIdFor;


    CommandEnumeration(int commandValue, boolean data, boolean dataval, int responseId, int responseIdFor, String commandNaming) {
        this.commandNaming = commandNaming;
        this.commandValue = (short) commandValue;
        this.data = data;
        this.dataval = dataval;
        this.responseId = (short) responseId;
        this.responseIdFor = (short) responseIdFor;
    }

    public boolean hasData() {
        return data;
    }

    public boolean hasDataval() {
        return dataval;
    }

    public CommandEnumerationDto toDto(){
        return new CommandEnumerationDto(commandNaming, commandValue, data, dataval, responseId, responseIdFor);
    }

    public static CommandEnumeration getCommandById(short id){
        return shortToCommandEnum.getOrDefault(id, DEFAULT);
    }
}
