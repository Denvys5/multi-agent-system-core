/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.controller;

import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.agent.StatusEntity;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class AgentStatusController {

    protected final ExecutorService executorService;

    protected IntellectualAgent intellectualAgent;

    public AgentStatusController() {
        executorService = Executors.newCachedThreadPool();
    }

    public void setIntellectualAgent(IntellectualAgent intellectualAgent) {
        this.intellectualAgent = intellectualAgent;
    }

    public void initialize() {
        intellectualAgent.setStatus(IEnumerationHelper.defaultStatusValue);
    }

    public StatusEntity getCurrentStatus() {
        return intellectualAgent.getStatus();
    }

    public void setStatus(StatusEntity status){
        intellectualAgent.setStatus(status);
        callStatusChangeEventListeners(status);
    }

    protected final ArrayList<Consumer<StatusEntity>> statusChangeEventListeners = new ArrayList<>();

    public void addStatusChangeEventListener(Consumer<StatusEntity> listener){
        statusChangeEventListeners.add(listener);
    }
    public void callStatusChangeEventListeners(StatusEntity status){
        for(Consumer<StatusEntity> listener: statusChangeEventListeners)
            executorService.submit(() -> listener.accept(status));
    }
    public void removeStatusChangeEventListener(Consumer<StatusEntity> listener){
        statusChangeEventListeners.remove(listener);
    }
}
