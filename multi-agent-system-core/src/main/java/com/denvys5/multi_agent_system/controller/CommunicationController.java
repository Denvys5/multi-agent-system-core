/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.controller;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.CommunicationInterfaceUtils;
import com.denvys5.multi_agent_system.service.event.AgentEventService;
import com.denvys5.multi_agent_system.service.message_io.IMessageReceiveFactory;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Setter
@Slf4j
public class CommunicationController {
    protected IMultiAgentSystemConfiguration configController;
    protected IMessageReceiveFactory messageReceiveFactory;
    protected AgentEventService agentEventService;
    protected IMessageSerializationService serializationService;
    protected CommunicationInterfaceUtils communicationInterfaceUtils;

    protected List<AgentCommunicationData> communicationDataList;
    @Getter
    @Setter(AccessLevel.NONE)
    protected List<AgentCommunicationInterface> communicationInterfaces = new ArrayList<>();

    public void resetInputs(){
        //TODO Refresh and reconnect
        List<AgentCommunicationInterface> newComms = new ArrayList<>();
        for(AgentCommunicationInterface comms: communicationInterfaces){
            if(communicationDataList.stream().noneMatch(value->value.getCommsType() == comms.getCommunicationData().getCommsType())){
                try {
                    comms.disconnect();
                } catch (Exception e) {
                    if(configController.isDebug())
                        log.error("Error disconnecting communication interface {}", comms, e);
                }
            }else{
                newComms.add(comms);
            }
        }

        for(AgentCommunicationData comms:communicationDataList){
            AgentCommunicationInterface newComm = communicationInterfaceUtils.getAgentCommunicationInterfaceFromCommsData(comms);
            if(Objects.nonNull(newComm)){
                newComm.setMessageSerializationService(serializationService);
                newComm.setConfigController(configController);
                newComm.thenConnected((client) -> {
                    try {
                        agentEventService.callCommunicationInterfaceConnectEventListeners(comms, client);
                        client.subscribeToInput(messageReceiveFactory::processIncomingMessage);
                        client.subscribeToBroadcast(messageReceiveFactory::processIncomingBroadcastMessage);
                    } catch (NetworkException e) {
                        if(configController.isDebug()){
                            log.error("Something went wrong during thenConnected call", e);
                        }
                    }
                });
                newComms.add(newComm);
                newComm.init();
                newComm.connect();
            }
        }

        communicationInterfaces = newComms;
    }

    public AgentCommunicationInterface getCommunicationInterface(AgentCommunicationData comms){
        return communicationInterfaces.stream()
                .filter(value->value.getCommunicationData().equals(comms))
                .findAny()
                .orElse(null);
    }

    public void sendMessageByCommsData(AgentCommunicationData comms, BasicAgentMessageDto message){
        AgentCommunicationInterface comm = getCommunicationInterface(comms);
        if(Objects.nonNull(comm)){
            if(comm.isConnected()){
                comm.sendMessageByCommsData(comms.getCommsData(), getStringFromMessage(message));
            }else{
                comm.thenConnected((c) -> c.sendMessageByCommsData(comms.getCommsData(), getStringFromMessage(message)));
            }
        }else{
            if(configController.isDebug())
                log.error("Communication interface " + comms.getCommsType().getCommsNaming() + "not found");
            sendBroadcastByHighestPriorityCommType(message);
        }
    }

    public Optional<AgentCommunicationData> getHighestPriorityCommsMethod(){
        return communicationDataList.stream()
                .max(Comparator.comparingInt(AgentCommunicationData::getPriority));
    }

    public void sendBroadcastByHighestPriorityCommType(BasicAgentMessageDto message){
        communicationInterfaces.stream()
                .filter(AgentCommunicationInterface::isConnected)
                .max(Comparator.comparingInt(value->value.getCommunicationData().getPriority()))
                .ifPresent(value ->
                        sendBroadcastByCommType(
                                value.getCommunicationData(),
                                message)
                );
    }

    public void sendBroadcastByCommType(AgentCommunicationData comms, BasicAgentMessageDto message){
        AgentCommunicationInterface comm = getCommunicationInterface(comms);
        if(Objects.nonNull(comm)){
            if(comm.isConnected()){
                comm.broadcastMessageToAgents(getStringFromMessage(message));
            }else{
                comm.thenConnected((c) -> comm.broadcastMessageToAgents(getStringFromMessage(message)));
            }
        }else{
            if(configController.isDebug())
                log.error("Communication interface " + comms + "not found");
            sendBroadcastByHighestPriorityCommType(message);
        }
    }

    public Optional<AgentCommunicationData> selectCommunicationMethodBySampleList(List<AgentCommunicationData> comms){
        return comms.stream()
                .filter(communicationDataList::contains)
                .max(Comparator.comparingInt(AgentCommunicationData::getPriority));
    }

    protected String getStringFromMessage(BasicAgentMessageDto messageDto){
        return serializationService.getStringFromBasicAgentMessage(messageDto);
    }

    public void shutdown(){
        for (AgentCommunicationInterface comms: communicationInterfaces){
            try {
                comms.disconnect();
            } catch (NetworkException e) {
                if(configController.isDebug())
                    log.error("Communication interface {} shutdown error", comms, e);
            }
        }
    }
}
