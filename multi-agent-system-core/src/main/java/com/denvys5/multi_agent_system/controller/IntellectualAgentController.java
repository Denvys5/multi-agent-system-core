/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.controller;

import com.denvys5.crypto.CryptoService;
import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.config.*;
import com.denvys5.multi_agent_system.config.impl.FileCommunicationConfiguration;
import com.denvys5.multi_agent_system.config.impl.FileMultiAgentNetworkConfiguration;
import com.denvys5.multi_agent_system.config.impl.FileMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.service.*;
import com.denvys5.multi_agent_system.service.event.AgentEventService;
import com.denvys5.multi_agent_system.service.message_io.*;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;
import com.denvys5.multi_agent_system.service.message_io.MultiAgentSystemConvertUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

@Setter
@Getter
@Slf4j
public class IntellectualAgentController {
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected final ExecutorService executor;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected final OnEventFutureService onEventFutureService;

    protected final IntellectualAgent intellectualAgent;

    public IntellectualAgentController(String agentType) {
        this(AgentEnumeration.valueOf(agentType));
    }

    public IntellectualAgentController(AgentEnumeration agentType) {
        executor = Executors.newSingleThreadExecutor();
        onEventFutureService = new OnEventFutureService(100);
        intellectualAgent = new IntellectualAgent();
        intellectualAgent.setCommsMethods(new ArrayList<>());
        intellectualAgent.setAgentEnumeration(agentType);
    }

    @Getter(AccessLevel.NONE)
    protected Class<?> relativeClass;
    @Getter(AccessLevel.NONE)
    protected boolean devMode = true;
    protected IMultiAgentSystemConfiguration configController;
    protected ICommunicationConfiguration communicationConfiguration;
    protected IMultiAgentNetworkConfiguration multiAgentNetworkConfiguration;
    protected CommunicationController communicationController;
    protected MultiAgentNetworkController multiAgentNetworkController;
    protected AgentDiscoveryController agentDiscoveryController;
    protected IMessageReceiveFactory messageReceiveFactory;
    protected IMessageProcessor agentMessageProcessor;
    protected MessageReceiveEventService messageReceiveEventService;
    protected HandshakeFactory handshakeFactory;
    protected IMessageFactory agentMessageFactory;
    protected AgentStatusController agentStatusController;
    protected AgentRequestIdTrackingService agentRequestIdTrackingService;
    protected AgentEventService agentEventService;
    protected IMessageSerializationService messageSerializationService;
    protected MultiAgentSystemConvertUtils multiAgentSystemConvertUtils;
    protected CommunicationInterfaceUtils communicationInterfaceUtils;
    protected IShutdownService shutdownService;
    protected ICryptoService cryptoService;
    protected IDataCommandProcessor dataCommandProcessor;
    protected IDataMessageFactory dataMessageFactory;
    protected IEnumerationHelper agentEnumerationHelper;

    public String getAgentUuid() {
        return intellectualAgent.getAgentUuid();
    }

    public String getAgentAlias() {
        return intellectualAgent.getAliasName();
    }

    public String isAgentSecureCommunication() {
        return intellectualAgent.getAliasName();
    }

    public AgentEnumeration getAgentEnumeration() {
        return intellectualAgent.getAgentEnumeration();
    }

    public List<AgentCommunicationData> getCommsMethods() {
        return intellectualAgent.getCommsMethods();
    }

    public void start(){
        executor.submit(() -> {
            try{
                this.init();
            } catch (Exception e) {
                log.error("Could not initialize intellectual agent {}", intellectualAgent, e);
            }
        });
    }

    public void init(){
        if(Objects.isNull(configController))
            configController = new FileMultiAgentSystemConfiguration(relativeClass, devMode);
        configController.init();

        if(Objects.isNull(cryptoService))
            cryptoService = new CryptoService();
        cryptoService.generateKeyPair();

        if(Objects.isNull(communicationConfiguration))
            communicationConfiguration = new FileCommunicationConfiguration(relativeClass, devMode);
        communicationConfiguration.init();

        if(Objects.isNull(multiAgentNetworkConfiguration))
            multiAgentNetworkConfiguration = new FileMultiAgentNetworkConfiguration(relativeClass, devMode);
        multiAgentNetworkConfiguration.init();

        if(Objects.isNull(messageSerializationService))
            messageSerializationService = new MessageSerializationService();

        if(Objects.isNull(multiAgentSystemConvertUtils))
            multiAgentSystemConvertUtils = new MultiAgentSystemConvertUtils();
        multiAgentSystemConvertUtils.setMessageSerializationService(messageSerializationService);

        if(Objects.isNull(communicationInterfaceUtils))
            communicationInterfaceUtils = new CommunicationInterfaceUtils();
        communicationInterfaceUtils.setSerializationService(messageSerializationService);

        intellectualAgent.setAgentUuid(configController.getAgentUUID());
        intellectualAgent.setSecureCommunication(configController.isSecureCommunication());
        intellectualAgent.setAliasName(configController.getAgentNameAlias());
        intellectualAgent.setCommsMethods(multiAgentSystemConvertUtils
                .getAgentCommunicationDataListFromDtos(
                        communicationConfiguration.getAgentCommunicationConfiguration()
                                .getComms()
                )
        );

        if(Objects.isNull(agentEnumerationHelper))
            agentEnumerationHelper = new AgentEnumerationHelper();
        agentEnumerationHelper.setThisAgentEnumeration(getAgentEnumeration());
        agentEnumerationHelper.init();

        if(Objects.isNull(agentEventService))
            agentEventService = new AgentEventService();

        if(Objects.isNull(agentStatusController))
            agentStatusController = new AgentStatusController();
        agentStatusController.setIntellectualAgent(intellectualAgent);
        agentStatusController.initialize();

        if(Objects.isNull(agentRequestIdTrackingService))
            agentRequestIdTrackingService = new AgentRequestIdTrackingService();
        agentRequestIdTrackingService.setConfigController(configController);
        agentRequestIdTrackingService.start();

        if(Objects.isNull(agentDiscoveryController))
            agentDiscoveryController = new AgentDiscoveryController();
        agentDiscoveryController.setConfigController(configController);
        agentDiscoveryController.setMessageSerializationService(messageSerializationService);
        agentDiscoveryController.setCommunicationInterfaceUtils(communicationInterfaceUtils);
        agentDiscoveryController.setAgentRequestIdTrackingService(agentRequestIdTrackingService);
        agentDiscoveryController.setCryptoService(cryptoService);

        if(Objects.isNull(multiAgentNetworkController))
            multiAgentNetworkController = new MultiAgentNetworkController();
        multiAgentNetworkController.setConfigController(configController);
        multiAgentNetworkController.setMultiAgentNetworkConfiguration(multiAgentNetworkConfiguration);
        multiAgentNetworkController.setMultiAgentSystemConvertUtils(multiAgentSystemConvertUtils);
        multiAgentNetworkController.setAgentEventService(agentEventService);
        multiAgentNetworkController.setCryptoService(cryptoService);
        multiAgentNetworkController.init();

        agentDiscoveryController.setMultiAgentNetworkController(multiAgentNetworkController);

        if(Objects.isNull(communicationController))
            communicationController = new CommunicationController();
        communicationController.setConfigController(configController);
        communicationController.setCommunicationInterfaceUtils(communicationInterfaceUtils);
        communicationController.setAgentEventService(agentEventService);
        communicationController.setSerializationService(messageSerializationService);
        communicationController.setCommunicationDataList(intellectualAgent.getCommsMethods());

        multiAgentNetworkController.setCommunicationController(communicationController);
        agentDiscoveryController.setCommunicationController(communicationController);

        if(Objects.isNull(handshakeFactory))
            handshakeFactory = new HandshakeFactory();
        handshakeFactory.setAgentCommunicationDatas(intellectualAgent.getCommsMethods());

        if(Objects.isNull(dataMessageFactory))
            dataMessageFactory = new SimpleDataMessageFactory();
        dataMessageFactory.setThisDeviceUuid(intellectualAgent.getAgentUuid());

        if(Objects.isNull(agentMessageFactory))
            agentMessageFactory = new AgentMessageFactory();
        agentMessageFactory.setThisDeviceUuid(intellectualAgent.getAgentUuid());
        agentMessageFactory.setThisDeviceType(intellectualAgent.getAgentEnumeration().getAgentTypeSet().getTypeValue());
        agentMessageFactory.setThisDeviceSecureCommunication(intellectualAgent.isSecureCommunication());
        agentMessageFactory.setThisDeviceAlias(intellectualAgent.getAliasName());
        agentMessageFactory.setHandshakeFactory(handshakeFactory);
        agentMessageFactory.setCryptoService(cryptoService);
        agentMessageFactory.setDataMessageFactory(dataMessageFactory);
        agentMessageFactory.setSerializationService(messageSerializationService);

        multiAgentNetworkController.setMessageFactory(agentMessageFactory);

        agentDiscoveryController.setAgentMessageFactory(agentMessageFactory);

        if(Objects.isNull(messageReceiveEventService))
            messageReceiveEventService = new MessageReceiveEventService();

        agentDiscoveryController.setMessageReceiveEventService(messageReceiveEventService);
        agentDiscoveryController.init();

        if(Objects.isNull(shutdownService))
            shutdownService = new ShutdownService();
        shutdownService.setShutdownSequence(this::shutdown);

        if(Objects.isNull(dataCommandProcessor))
            dataCommandProcessor = new SimpleDataMessageProcessor();
        dataCommandProcessor.setConfigController(configController);
        dataCommandProcessor.setMultiAgentNetworkController(multiAgentNetworkController);
        dataCommandProcessor.setHandshakeFactory(handshakeFactory);
        dataCommandProcessor.setAgentMessageFactory(agentMessageFactory);
        dataCommandProcessor.setStatusController(agentStatusController);
        dataCommandProcessor.setCryptoService(cryptoService);
        dataCommandProcessor.setSerializationService(messageSerializationService);
        dataCommandProcessor.setAgentEnumerationHelper(agentEnumerationHelper);
        dataCommandProcessor.setShutdownService(shutdownService);

        if(Objects.isNull(agentMessageProcessor))
            agentMessageProcessor = new AgentMessageProcessor();
        agentMessageProcessor.setConfigController(configController);
        agentMessageProcessor.setMessageReceiveEventService(messageReceiveEventService);
        agentMessageProcessor.setMultiAgentNetworkController(multiAgentNetworkController);
        agentMessageProcessor.setHandshakeFactory(handshakeFactory);
        agentMessageProcessor.setAgentMessageFactory(agentMessageFactory);
        agentMessageProcessor.setStatusController(agentStatusController);
        agentMessageProcessor.setSerializationService(messageSerializationService);
        agentMessageProcessor.setCryptoService(cryptoService);
        agentMessageProcessor.setMultiAgentSystemConvertUtils(multiAgentSystemConvertUtils);
        agentMessageProcessor.setDataCommandProcessor(dataCommandProcessor);
        agentMessageProcessor.setAgentEnumerationHelper(agentEnumerationHelper);

        if(Objects.isNull(messageReceiveFactory))
            messageReceiveFactory = new MessageReceiveFactory();
        messageReceiveFactory.setConfigController(configController);
        messageReceiveFactory.setMessageReceiveEventService(messageReceiveEventService);
        messageReceiveFactory.setAgentRequestIdTrackingService(agentRequestIdTrackingService);
        messageReceiveFactory.setMessageSerializationService(messageSerializationService);
        messageReceiveFactory.setThisAgentEnumeration(getAgentEnumeration());
        messageReceiveFactory.setThisAgentId(intellectualAgent.getAgentUuid());
        messageReceiveFactory.setMessageProcessor(agentMessageProcessor);
        messageReceiveFactory.setAgentEnumerationHelper(agentEnumerationHelper);

        communicationController.setMessageReceiveFactory(messageReceiveFactory);
        communicationController.resetInputs();

        multiAgentNetworkController.enableWorkers();
        agentDiscoveryController.enableDiscoverySchedule();

        log.info("Agent has been initialized {}", intellectualAgent);

        onEventFutureService.setFlag();
    }

    public void thenReady(AgentThenReadyExecutor executor){
        CompletableFuture<Void> completableFuture = this.onEventFutureService.get();
        completableFuture.thenRun(() -> executor.thenReady(this));
    }

    public void untilReadyBlock() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> completableFuture = this.onEventFutureService.get();
        completableFuture.get();
    }

    public void untilReadyBlock(long timeout, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture<Void> completableFuture = this.onEventFutureService.get();
        completableFuture.get(timeout, timeUnit);
    }

    public void shutdown(){
        log.info("Shutting down {}", intellectualAgent);
        agentDiscoveryController.shutdown();
        multiAgentNetworkController.shutdown();
        communicationController.shutdown();
        agentRequestIdTrackingService.shutdown();
        messageReceiveEventService.shutdown();
    }
}
