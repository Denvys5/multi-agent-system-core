/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.controller;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.config.IMultiAgentNetworkConfiguration;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.*;
import com.denvys5.multi_agent_system.model.enumeration.AgentTypeSetEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommunicationMethodEnumeration;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.message_io.MultiAgentSystemConvertUtils;
import com.denvys5.multi_agent_system.service.event.AgentEventService;
import com.denvys5.multi_agent_system.service.message_io.IMessageFactory;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Setter
@Slf4j
public class MultiAgentNetworkController {
    protected IMultiAgentSystemConfiguration configController;
    protected IMultiAgentNetworkConfiguration multiAgentNetworkConfiguration;
    protected MultiAgentSystemConvertUtils multiAgentSystemConvertUtils;
    protected CommunicationController communicationController;
    protected IMessageFactory messageFactory;
    protected AgentEventService agentEventService;
    protected ICryptoService cryptoService;

    protected final Object agentLock = new Object();

    @Getter
    protected MultiAgentNetworkConfiguration multiAgentNetwork;
    protected MultiAgentNetworkConfiguration oldMultiAgentNetwork;
    protected final ScheduledExecutorService agentActivityWorker = Executors.newSingleThreadScheduledExecutor();
    protected final ScheduledExecutorService agentDisconnectWorker = Executors.newSingleThreadScheduledExecutor();
    protected final ScheduledExecutorService multiAgentSystemConfigWorker = Executors.newSingleThreadScheduledExecutor();

    @Setter(AccessLevel.NONE)
    protected int pingTimeout = 60000;
    @Setter(AccessLevel.NONE)
    protected int minimalInitialTimeout = 60000;
    @Setter(AccessLevel.NONE)
    protected int maximumTimeout = 150000;

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
        pingTimeout = configController.getPingTimeout();
        minimalInitialTimeout = configController.getMinimalInitialTimeout();
        maximumTimeout = configController.getMaximumTimeout();
    }

    public void init(){
        multiAgentNetwork = multiAgentSystemConvertUtils.getMultiAgentNetworkConfigurationFromDto(multiAgentNetworkConfiguration.getMultiAgentNetworkConfiguration());
        oldMultiAgentNetwork = new MultiAgentNetworkConfiguration(Collections.emptyList());

        multiAgentNetwork.getAgents().forEach(agent -> {
            try {
                agent.setPublicEcdsaKey(cryptoService.getPublicEcdsaKey(agent.getPublicEcdsaKeyB64()));
            } catch (Exception e) {
                if(configController.isDebug())
                    log.error(e.getMessage(), e);
            }
        });
    }

    public void enableWorkers(){
        initialRefreshMultiAgentNetwork();
        multiAgentSystemConfigWorker.scheduleWithFixedDelay(this::saveMultiAgentNetwork, minimalInitialTimeout, minimalInitialTimeout, TimeUnit.MILLISECONDS);
        agentActivityWorker.scheduleAtFixedRate(this::scanAgentsAndSendPing, minimalInitialTimeout, pingTimeout, TimeUnit.MILLISECONDS);
        agentDisconnectWorker.scheduleAtFixedRate(this::offlineDeadAgents, minimalInitialTimeout + pingTimeout, pingTimeout, TimeUnit.MILLISECONDS);
    }

    protected void initialRefreshMultiAgentNetwork() {
        multiAgentNetwork.getAgents().forEach(agent -> {
            communicationController.selectCommunicationMethodBySampleList(agent.getCommsMethods()).ifPresent(agentCommunicationData -> {
                try {
                    sendMessageToAgent(agent, messageFactory.getHandshakeRequestMessage(agentCommunicationData));
                }catch (Exception ignored){
                }
            });
        });
    }

    protected void scanAgentsAndSendPing(){
        List<IntellectualAgent> agents;
        synchronized (agentLock){
            long currentTime = System.currentTimeMillis();
            agents = multiAgentNetwork.getAgents()
                    .stream()
                    .filter(IntellectualAgent::isOnline)
                    .filter(agent ->
                            agent.getLastMessageReceived() < currentTime - minimalInitialTimeout)
                    .collect(Collectors.toList());
        }
        log.atDebug().setMessage("scanning {}").addArgument(() -> agents.stream().map(IntellectualAgent::getAgentUuid).collect(Collectors.toList())).log();
        agents.forEach(agent ->{
            try {
                sendMessageToAgent(agent, messageFactory.getPingRequestMessage());
            }catch (Exception ignored){
            }
        });
    }

    protected void offlineDeadAgents(){
        List<IntellectualAgent> agentsToOffline;
        synchronized (agentLock){
            long currentTime = System.currentTimeMillis();
            agentsToOffline = multiAgentNetwork.getAgents()
                    .stream()
                    .filter(IntellectualAgent::isOnline)
                    .filter(agent ->
                            agent.getLastMessageReceived() < currentTime - maximumTimeout)
                    .collect(Collectors.toList());
            agentsToOffline.forEach(agent-> agent.setOnline(false));
        }
        log.atDebug().setMessage("disconnecting {}").addArgument(() -> agentsToOffline.stream().map(IntellectualAgent::getAgentUuid).collect(Collectors.toList())).log();
        agentsToOffline.forEach(agent -> agentEventService.callAgentOfflineEventListeners(agent));
    }

    //TODO do we need this?
    protected void disconnectDeadAgents(){
        List<IntellectualAgent> agentsToDisconnect;
        synchronized (agentLock){
            long currentTime = System.currentTimeMillis();
            agentsToDisconnect = multiAgentNetwork.getAgents()
                    .stream()
                    .filter(agent ->
                            agent.getLastMessageReceived() < currentTime - maximumTimeout)
                    .collect(Collectors.toList());
            multiAgentNetwork.getAgents()
                    .removeAll(agentsToDisconnect);
        }
        log.debug("disconnecting {}", agentsToDisconnect);
        agentsToDisconnect.forEach(agent -> agentEventService.callAgentDisconnectedEventListeners(agent));
    }

    public void saveMultiAgentNetwork(){
        synchronized (agentLock) {
            if(oldMultiAgentNetwork.equals(multiAgentNetwork)){
                return;
            }
            log.debug("saving {}", multiAgentNetwork);

            multiAgentNetworkConfiguration.setMultiAgentNetworkConfiguration(multiAgentSystemConvertUtils.getMultiAgentNetworkConfigurationDto(multiAgentNetwork));

            oldMultiAgentNetwork = multiAgentSystemConvertUtils.getMultiAgentNetworkConfigurationFromDto(multiAgentNetworkConfiguration.getMultiAgentNetworkConfiguration());
        }
    }

    public boolean addAgentToNetwork(IntellectualAgent agent){
        log.info("Adding agent {}", agent);
        if(!isAgentPresentById(agent.getAgentUuid())){
            synchronized (agentLock){
                log.info("Added agent {}", agent);
                multiAgentNetwork.getAgents().add(agent);
            }
            agentEventService.callAgentConnectedEventListeners(agent);
        }else
            return false;

        return true;
    }

    public boolean isAgentPresentById(String agentId){
        synchronized (agentLock){
            return multiAgentNetwork.getAgents().stream().anyMatch(intellectualAgent -> intellectualAgent.getAgentUuid().equals(agentId));
        }
    }

    public Optional<IntellectualAgent> getAgentById(String agentId){
        synchronized (agentLock) {
            return multiAgentNetwork.getAgents().stream().filter(intellectualAgent -> intellectualAgent.getAgentUuid().equals(agentId)).findAny();
        }
    }

    public void addCommunicationMethodToAgentById(String agentId, AgentCommunicationData communicationData){
        getAgentById(agentId).ifPresent(agent->{
            AgentCommunicationData target = null;
            for (AgentCommunicationData comm : agent.getCommsMethods()) {
                if (comm.getCommsType() == communicationData.getCommsType()) {
                    target = comm;
                    break;
                }
            }
            if(Objects.nonNull(target)){
                target.setCommsData(communicationData.getCommsData());
            }else{
                agent.getCommsMethods().add(communicationData);
            }
        });
    }

    public Optional<AgentCommunicationInterfaceDataDto> getAgentsCommunicationDataByIdAndType(String agentId, CommunicationMethodEnumeration type){
        return getAgentById(agentId)
                .flatMap(value ->
                        value.getCommsMethods()
                                .stream()
                                .filter(method ->
                                        method.getCommsType() == type)
                                .findAny())
                .map(AgentCommunicationData::getCommsData);
    }

    public void callbackAgentById(String agentId, Consumer<IntellectualAgent> callback){
        getAgentById(agentId).ifPresent(callback);
    }

    public void sendMessageToAgentById(String agentId, BasicAgentMessageDto message){
        getAgentById(agentId).ifPresent(agent -> sendMessageToAgent(agent, message));
    }

    public void sendMessageToAgentByCommsData(AgentCommunicationData comms, BasicAgentMessageDto message){
        communicationController.sendMessageByCommsData(comms, message);
    }

    public void sendMessageToAgent(IntellectualAgent agent, BasicAgentMessageDto message){
        Optional<AgentCommunicationData> communicationDataOptional = communicationController.selectCommunicationMethodBySampleList(agent.getCommsMethods());
        message.setToAgent(agent.getAgentUuid());
        if(communicationDataOptional.isPresent()){
            sendMessageToAgentByCommsData(communicationDataOptional.get(), message);
        }else{
            sendMessageToAgentWithNoAvailableComms(message);
        }
    }

    public void sendMessageToAgentWithNoAvailableComms(BasicAgentMessageDto message){
        communicationController.sendBroadcastByHighestPriorityCommType(message);
    }

    public void sendMessageToAgentsByType(AgentTypeSetEnumeration agentType, BasicAgentMessageDto message){
        getMultiAgentNetwork()
                .getAgents()
                .stream()
                .filter(agent ->
                        agent.getAgentEnumeration().getAgentTypeSet() == agentType)
                .forEach((agent) -> {
                        try {
                            sendMessageToAgent(agent, message);
                        } catch (NetworkException ex) {
                            if(configController.isDebug())
                                log.error("Failed to send message to agents", ex);
                        }
                    }
                );
    }

    public void callbackAgentsByType(AgentTypeSetEnumeration agentType, Consumer<IntellectualAgent> callback){
        getMultiAgentNetwork()
                .getAgents()
                .stream()
                .filter(agent ->
                        agent.getAgentEnumeration().getAgentTypeSet() == agentType)
                .forEach(callback);
    }

    public void shutdown(){
        agentActivityWorker.shutdownNow();
        agentDisconnectWorker.shutdownNow();
        multiAgentSystemConfigWorker.shutdownNow();
    }
}
