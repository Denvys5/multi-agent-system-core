/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.controller;

import com.denvys5.crypto.CryptographicException;
import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.*;
import com.denvys5.multi_agent_system.model.message.*;
import com.denvys5.multi_agent_system.service.AgentRequestIdTrackingService;
import com.denvys5.multi_agent_system.service.CommunicationInterfaceUtils;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.message_io.IMessageFactory;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.*;

@Setter
@Slf4j
public class AgentDiscoveryController {
    @Setter(AccessLevel.NONE)
    protected ScheduledFuture<?> discoverySchedule;

    protected final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    protected CommunicationController communicationController;
    protected MessageReceiveEventService messageReceiveEventService;
    protected MultiAgentNetworkController multiAgentNetworkController;
    protected IMultiAgentSystemConfiguration configController;
    protected IMessageFactory agentMessageFactory;
    protected AgentRequestIdTrackingService agentRequestIdTrackingService;
    protected ICryptoService cryptoService;
    protected IMessageSerializationService messageSerializationService;
    protected CommunicationInterfaceUtils communicationInterfaceUtils;

    @Setter(AccessLevel.NONE)
    protected long timeout = 60000;

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
        timeout = configController.getAgentDiscoveryTimeOut();
    }

    public void init(){
        this.messageReceiveEventService.getListenerGroupByCommand(CommandEnumeration.AGENT_DISCOVERY_BROADCAST).addEventListener(this::onDiscoveryBroadcastReceived);
        this.messageReceiveEventService.getListenerGroupByCommand(CommandEnumeration.HANDSHAKE).addEventListener(this::onHandshakeRequestReceived);
    }

    public void enableDiscoverySchedule(){
        if(Objects.isNull(discoverySchedule)){
            discoverySchedule = executorService.scheduleWithFixedDelay(this::agentDiscoveryJob, 0, timeout, TimeUnit.MILLISECONDS);
        }
    }

    public void disableDiscoverySchedule(){
        if(Objects.nonNull(discoverySchedule)){
            discoverySchedule.cancel(false);
            discoverySchedule = null;
        }
    }

    protected void agentDiscoveryJob(){
        communicationController.getCommunicationInterfaces()
                .forEach(agentCommunicationInterface -> {
                    AgentCommunicationData agentCommunicationData = agentCommunicationInterface.getCommunicationData();
                    communicationController.sendBroadcastByCommType(
                            agentCommunicationData,
                            agentMessageFactory.getAgentDiscoveryMessage(agentCommunicationData)
                    );
                });
    }

    protected void onHandshakeResponseReceived(BasicAgentMessage response, IntellectualAgent agent){
        switch (CommandResultStatus.getResultStatusFromCode(response.getDataVal())){
            case SUCCESS:{
                boolean addAgentToNetwork = multiAgentNetworkController.addAgentToNetwork(agent);
                if(!addAgentToNetwork){
                    if(configController.isDebug()){
                        log.error("Something went wrong in agent handshake processing: {}", CommandResultStatus.SUCCESS);
                    }
                }
            } break;
            case TOO_MANY_REQUESTS:{
                if(!multiAgentNetworkController.isAgentPresentById(agent.getAgentUuid())){
                    boolean addAgentToNetwork = multiAgentNetworkController.addAgentToNetwork(agent);
                    if(!addAgentToNetwork){
                        if(configController.isDebug()){
                            log.error("Something went wrong in agent handshake processing: {}", CommandResultStatus.TOO_MANY_REQUESTS);
                        }
                    }
                }
            } break;
            case DEFAULT:
            case SERVICE_UNAVAILABLE:
            case BAD_GATEWAY:
            case NOT_IMPLEMENTED:
            case INTERNAL_SERVER_ERROR:
            case NOT_FOUND:
            case FORBIDDEN:
            case UNAUTHORIZED:
            case BAD_REQUEST:{
                if(configController.isDebug()){
                    log.error("Handshake with " + agent.getAliasName() + "-" + agent.getAgentUuid() + " failed with status " + response.getDataVal());
                }
            } break;
        }
    }

    protected void onDiscoveryBroadcastReceived(AgentCommunicationData readFrom, BasicAgentMessage message){
        if(multiAgentNetworkController.isAgentPresentById(message.getFromAgent())){
            IntellectualAgent agent = multiAgentNetworkController.getAgentById(message.getFromAgent()).get();
            if(agent.getCommsMethods()
                    .stream()
                    .noneMatch(communicationData ->
                            communicationData.equals(readFrom)
                    )){
                updateCommunicationMethodsOfAgent(agent, readFrom, message);
            }
            updateKeys(message, agent);
            return;
        }

        IntellectualAgent agent = new IntellectualAgent();
        agent.setCommsMethods(new ArrayList<>());

        try {
            initAgent(message, agent);
        }catch (Exception e){
            return;
        }

        AgentCommunicationData targetCommunicationData = communicationInterfaceUtils.getAgentCommunicationDataFromInterchangeMessage(readFrom, message);

        if(Objects.nonNull(targetCommunicationData)){
            agent.getCommsMethods().add(targetCommunicationData);

            BasicAgentMessageDto messageDto = agentMessageFactory.getHandshakeRequestMessage(readFrom);
            messageDto.setToAgent(message.getFromAgent());
            try {
                multiAgentNetworkController.sendMessageToAgentByCommsData(targetCommunicationData, messageDto);
            } catch (NetworkException e) {
                if (configController.isDebug())
                    log.error(e.getMessage(), e);
            }

            agentRequestIdTrackingService.addRequestListener(messageDto.getRequestID(), (response) -> onHandshakeResponseReceived(response, agent));
        }
    }

    protected void updateKeys(BasicAgentMessage message, IntellectualAgent agent) {
        if(!agent.isOnline() || Objects.isNull(agent.getPublicEcdhKeyB64()) || Objects.isNull(agent.getPublicEcdsaKey())){
            log.info("Updating keys {} offline={} ecdhIsNull={} ecdsaIsNull={}", agent.getAliasName(), !agent.isOnline(), Objects.isNull(agent.getPublicEcdhKeyB64()), Objects.isNull(agent.getPublicEcdsaKey()));
            try {
                AgentCommunicationInterchangeDto agentCommunicationInterchangeDto = messageSerializationService.deserializeObject(message.getData(), AgentCommunicationInterchangeDto.class);
                agent.setPublicEcdhKeyB64(agentCommunicationInterchangeDto.getPublicEcdhKey());
                agent.setSecretEcdhKey(cryptoService.getSecretEcdhKey(agentCommunicationInterchangeDto.getPublicEcdhKey()));
                agent.setPublicEcdsaKeyB64(agentCommunicationInterchangeDto.getPublicEcdsaKey());
                agent.setPublicEcdsaKey(cryptoService.getPublicEcdsaKey(agent.getPublicEcdsaKeyB64()));
            }catch (Exception e){
                if(configController.isDebug())
                    log.error(e.getMessage(), e);
            }
        }else{
            if(configController.isDebug())
                log.warn("Updating keys condition for {} is not met", agent.getAliasName());
        }
    }

    protected void initAgent(BasicAgentMessage message, IntellectualAgent agent) {
        AgentCommunicationInterchangeDto agentCommunicationInterchangeDto = messageSerializationService.deserializeObject(message.getData(), AgentCommunicationInterchangeDto.class);
        agent.setAgentUuid(message.getFromAgent());
        agent.setAliasName(agentCommunicationInterchangeDto.getAgentAlias());
        agent.setAgentEnumeration(AgentEnumeration.getAgentEnumerationByTypeSetValue(agentCommunicationInterchangeDto.getType()));
        agent.setStatus(IEnumerationHelper.defaultStatusValue);
        agent.setLastMessageReceived(System.currentTimeMillis());
        agent.setSecureCommunication(agentCommunicationInterchangeDto.isSecureCommunication());
        agent.setOnline(true);
        agent.setPublicEcdhKeyB64(agentCommunicationInterchangeDto.getPublicEcdhKey());
        agent.setPublicEcdsaKeyB64(agentCommunicationInterchangeDto.getPublicEcdsaKey());
        try {
            agent.setSecretEcdhKey(cryptoService.getSecretEcdhKey(agent.getPublicEcdhKeyB64()));
            agent.setPublicEcdsaKey(cryptoService.getPublicEcdsaKey(agent.getPublicEcdsaKeyB64()));
        } catch (CryptographicException e) {
            if(configController.isDebug())
                log.error(e.getMessage(), e);
            agent.setSecretEcdhKey(null);
            agent.setPublicEcdsaKey(null);
        }
    }

    protected void updateCommunicationMethodsOfAgent(IntellectualAgent agent, AgentCommunicationData readFrom, BasicAgentMessage message){
        AgentCommunicationData targetCommunicationData = communicationInterfaceUtils.getAgentCommunicationDataFromInterchangeMessage(readFrom, message);
        if(Objects.nonNull(targetCommunicationData)){
            agent.getCommsMethods().add(targetCommunicationData);
        }
    }

    protected void updateCommunicationMethodsOfAgent(IntellectualAgent agent, AgentCommunicationData readFrom, AgentCommunicationInterfaceDataDto agentCommunicationInterfaceDataDto){
        agent.getCommsMethods().add(new AgentCommunicationData((byte) 1, readFrom.getCommsType(), agentCommunicationInterfaceDataDto));
    }

    public void onHandshakeRequestReceived(AgentCommunicationData readFrom, BasicAgentMessage message){
        CommandResultStatus status = CommandResultStatus.TOO_MANY_REQUESTS;

        String uuid = message.getFromAgent();
        boolean agentPresentFlag = multiAgentNetworkController.isAgentPresentById(uuid);

        IntellectualAgent agent = null;

        if(!agentPresentFlag){
            agent = new IntellectualAgent();
            agent.setCommsMethods(new ArrayList<>());
            try {
                initAgent(message, agent);
            }catch (Exception e){
                status = CommandResultStatus.BAD_REQUEST;
            }

            if(multiAgentNetworkController.addAgentToNetwork(agent)){
                status = CommandResultStatus.SUCCESS;
            }
        }

        AgentCommunicationInterfaceDataDto agentCommunicationInterfaceDataDto = communicationInterfaceUtils.getAgentCommunicationDataDtoFromHandshake(readFrom, message);

        if(Objects.nonNull(agentCommunicationInterfaceDataDto)){
            if(agentPresentFlag){
                agent = multiAgentNetworkController.getAgentById(uuid).get();
                if(agent.getCommsMethods()
                        .stream()
                        .noneMatch(communicationData ->
                                communicationData.equals(readFrom)
                        )){
                    updateCommunicationMethodsOfAgent(agent, readFrom, agentCommunicationInterfaceDataDto);
                }
                updateKeys(message, agent);
                //Fix to synchronisation issue with AgentMessageProcessor
                agent.setOnline(true);
            }else{
                agent.getCommsMethods().add(new AgentCommunicationData((byte) 1, readFrom.getCommsType(), agentCommunicationInterfaceDataDto));
            }
        }else{
            status = CommandResultStatus.NOT_IMPLEMENTED;
        }

        if(status == CommandResultStatus.SUCCESS){
            try {
                multiAgentNetworkController.sendMessageToAgent(agent, agentMessageFactory.getHandshakeResponseMessage(message.getRequestID(), status.getResultValue()));
            } catch (NetworkException e) {
                if(configController.isDebug())
                    log.error(e.getMessage(), e);
            }
        }else if(status != CommandResultStatus.NOT_IMPLEMENTED){
            try {
                BasicAgentMessageDto messageDto = agentMessageFactory.getHandshakeResponseMessage(message.getRequestID(), status.getResultValue());
                messageDto.setToAgent(message.getFromAgent());
                multiAgentNetworkController.sendMessageToAgentByCommsData(
                        new AgentCommunicationData((byte) 0, readFrom.getCommsType(), agentCommunicationInterfaceDataDto),
                        messageDto
                );
            } catch (NetworkException e) {
                if(configController.isDebug())
                    log.error(e.getMessage(), e);
            }
        }
    }

    public void shutdown(){
        disableDiscoverySchedule();
        executorService.shutdown();
    }
}
