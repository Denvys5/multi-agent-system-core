/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.mqtt;

import com.denvys5.crypto.SslUtil;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.MqttAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.service.OnEventFutureService;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.IMessageListener;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import com.denvys5.multi_agent_system.service.network.NetworkOnConnectExecutor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import javax.net.ssl.SSLSocketFactory;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class MqttAgentCommunicationInterface extends Thread implements AgentCommunicationInterface {
    private IMultiAgentSystemConfiguration configController;
    private IMessageSerializationService messageSerializationService;
    private IMqttClient client;
    private final OnEventFutureService networkOnConnectFutureService = new OnEventFutureService(1000);
    private Optional<SSLSocketFactory> sslSocketFactory;
    private final Map<String, IMessageListener> subscribers = new HashMap<>();

    private AgentCommunicationData communicationData;
    private MqttAgentCommunicationInterfaceDataDto mqttCommData;

    public MqttAgentCommunicationInterface(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        mqttCommData = convertFromAbstract(communicationData.getCommsData());
    }

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
    }

    @Override
    public void setMessageSerializationService(IMessageSerializationService serializationService) {
        this.messageSerializationService = serializationService;
    }

    @Override
    public void setupConnectionProperties(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        mqttCommData = convertFromAbstract(communicationData.getCommsData());
    }

    public AgentCommunicationData getCommunicationData() {
        return communicationData;
    }

    @Override
    public void thenConnected(NetworkOnConnectExecutor networkOnConnectExecutor){
        CompletableFuture<Void> completableFuture = networkOnConnectFutureService.get();
        completableFuture.thenRun(() -> networkOnConnectExecutor.onConnect(this));
    }

    @Override
    public void init() {
        try {
            //TODO rework this somehow, this is utterly broken
            URL caCrt = ClassLoader
                    .getSystemClassLoader()
                    .getResource("ca.crt");
            URL clientCrt = ClassLoader
                    .getSystemClassLoader()
                    .getResource("client.crt");
            URL clientKey = ClassLoader
                    .getSystemClassLoader()
                    .getResource("client.key");
            if(Objects.nonNull(caCrt) && Objects.nonNull(clientCrt) && Objects.nonNull(clientKey)){
                sslSocketFactory = Optional.ofNullable(SslUtil
                        .getSocketFactory(
                                caCrt
                                        .getPath(),
                                clientCrt
                                        .getPath(),
                                clientKey
                                        .getPath(),
                                ""));
            }else{
                sslSocketFactory = Optional.empty();
            }
        } catch (Exception e) {
            sslSocketFactory = Optional.empty();
            if(configController.isDebug())
                log.error("Failed to initialize SSL certificates", e);
        }

        String publisherId = UUID.randomUUID().toString();
        try {
            String url = mqttCommData.isSsl() && sslSocketFactory.isPresent() ?
                    "ssl://" + mqttCommData.getHost() + ":" + mqttCommData.getPort() :
                    "tcp://" + mqttCommData.getHost() + ":" + mqttCommData.getPort();

            client = new MqttClient(url, publisherId, new MemoryPersistence());
        } catch (MqttException e) {
            if(configController.isDebug())
                log.error("Failed to initialize MQTT client", e);
        }
    }

    public void run(){
        synchronized (this){
            connector();
        }
    }

    @Override
    public void connect(){
        if(Objects.nonNull(client))
            this.start();
    }

    public synchronized void connector(){
        client.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectionLost(Throwable cause) {
                if(configController.isDebug())
                    log.error("Connection to MQTT server " + mqttCommData.getHost() + ":" + mqttCommData.getPort() + " is lost", cause);
            }
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {}
            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {}
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                subscribers.forEach((key, value)->{
                    subscribe(key, value);
                });
            }
        });
        try {
            client.connect(getConnectionOptions());
        } catch (MqttException e) {
            if(configController.isDebug())
                log.error("Connecting to MQTT server " + mqttCommData.getHost() + ":" + mqttCommData.getPort() + " failed", e);
        }
        while(!client.isConnected()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }

        networkOnConnectFutureService.setFlag();
    }

    private MqttConnectOptions getConnectionOptions(){
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(configController.getReconnectDelay()/1000);
        options.setUserName(mqttCommData.getLogin());
        options.setPassword(mqttCommData.getPassword().toCharArray());
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
        sslSocketFactory.ifPresent(ssl -> {
            if(mqttCommData.isSsl())
                options.setSocketFactory(ssl);
        });
        return options;
    }

    @Override
    public boolean isConnected(){
        return Objects.nonNull(client) && client.isConnected();
    }

    public IMqttClient getClient(){
        return client;
    }

    public void publish(String topic, String message) throws NetworkException {
        if(configController.isVerboseCommunication())
            log.info("Mqtt publish to {}: {}", topic, message);
        publish(topic, messageSerializationService.getBytesFromString(message));
    }

    @Override
    public void sendMessageByCommsData(AgentCommunicationInterfaceDataDto commsData, String message) throws NetworkException {
        MqttAgentCommunicationInterfaceDataDto sendTo = convertFromAbstract(commsData);
        publish(sendTo.getInputTopic(), message);
    }

    @Override
    public void broadcastMessageToAgents(String message) throws NetworkException {
        publish(mqttCommData.getBroadcastTopic(), message);
    }

    public void publish(String topic, byte[] payload) throws NetworkException {
        try {
            MqttMessage msg = new MqttMessage(payload);
            msg.setQos(2);
            client.publish(topic, msg);
        }catch (MqttException e){
            throw new NetworkException(e);
        }
    }

    @Override
    public void subscribe(AgentCommunicationInterfaceDataDto commsData, IMessageListener listener) throws NetworkException {
        MqttAgentCommunicationInterfaceDataDto subscribeTo = convertFromAbstract(commsData);
        subscribe(subscribeTo.getInputTopic(), listener);
    }

    @Override
    public void subscribeToInput(IMessageListener listener) throws NetworkException {
        subscribe(mqttCommData.getInputTopic(), listener);
    }

    @Override
    public void subscribeToBroadcast(IMessageListener listener) throws NetworkException {
        subscribe(mqttCommData.getBroadcastTopic(), listener);
    }

    @Override
    public void unsubscribe() throws NetworkException {
        unsubscribe(mqttCommData.getBroadcastTopic());
        unsubscribe(mqttCommData.getInputTopic());
    }

    public void unsubscribe(String topic) throws NetworkException{
        try {
            client.unsubscribe(topic);
            subscribers.remove(topic);
        } catch (MqttException e) {
            throw new NetworkException(e);
        }
    }

    public void subscribe(String topic, IMessageListener listener) throws NetworkException {
        try {
            client.subscribe(topic, (from, msg) -> {
                String payload = messageSerializationService.getStringFromPayload(msg.getPayload());
                listener.onMessageReceived(this.communicationData, payload);
            });
            if(!subscribers.containsKey(topic))
                subscribers.put(topic, listener);
        } catch (MqttException e) {
            throw new NetworkException(e);
        }
    }

    @Override
    public void disconnect() throws NetworkException{
        if(Objects.nonNull(client)){
            try {
                client.disconnect();
            } catch (MqttException e) {
                if(configController.isDebug())
                    log.error("Client is already disconnected", e);
                throw new NetworkException(e);
            }
        }
    }

    private MqttAgentCommunicationInterfaceDataDto convertFromAbstract(AgentCommunicationInterfaceDataDto communicationData){
        return (MqttAgentCommunicationInterfaceDataDto)communicationData;
    }
}
