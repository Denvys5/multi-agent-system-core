/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.event;

import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class AgentEventService {
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final List<BiConsumer<AgentCommunicationData, AgentCommunicationInterface>> communicationInterfaceConnectEventListeners = new CopyOnWriteArrayList<>();
    public void addCommunicationInterfaceConnectEventListener(BiConsumer<AgentCommunicationData, AgentCommunicationInterface> listener){
        communicationInterfaceConnectEventListeners.add(listener);
    }
    public void callCommunicationInterfaceConnectEventListeners(AgentCommunicationData communicationData, AgentCommunicationInterface communicationInterface){
        executorService.submit(() ->
                communicationInterfaceConnectEventListeners.forEach((listener) -> listener.accept(communicationData, communicationInterface)));
    }
    public void removeCommunicationInterfaceConnectEventListener(BiConsumer<AgentCommunicationData, AgentCommunicationInterface> listener){
        communicationInterfaceConnectEventListeners.remove(listener);
    }

    private final List<Consumer<IntellectualAgent>> agentConnectedEventListeners = new CopyOnWriteArrayList<>();
    public void addAgentConnectedEventListener(Consumer<IntellectualAgent> listener){
        agentConnectedEventListeners.add(listener);
    }
    public void callAgentConnectedEventListeners(IntellectualAgent agent){
        executorService.submit(() ->
                agentConnectedEventListeners.forEach((listener) -> listener.accept(agent)));
    }
    public void removeAgentConnectedEventListener(Consumer<IntellectualAgent> listener){
        agentConnectedEventListeners.remove(listener);
    }

    private final List<Consumer<IntellectualAgent>> agentOfflineEventListeners = new CopyOnWriteArrayList<>();
    public void addAgentOfflineEventListener(Consumer<IntellectualAgent> listener){
        agentOfflineEventListeners.add(listener);
    }
    public void callAgentOfflineEventListeners(IntellectualAgent agent){
        executorService.submit(() -> agentOfflineEventListeners.forEach((listener) -> listener.accept(agent)));
    }
    public void removeAgentOfflineEventListener(Consumer<IntellectualAgent> listener){
        agentOfflineEventListeners.remove(listener);
    }

    private final List<Consumer<IntellectualAgent>> agentDisconnectedEventListeners = new CopyOnWriteArrayList<>();
    public void addAgentDisconnectedEventListener(Consumer<IntellectualAgent> listener){
        agentDisconnectedEventListeners.add(listener);
    }
    public void callAgentDisconnectedEventListeners(IntellectualAgent agent){
        executorService.submit(() -> agentDisconnectedEventListeners.forEach((listener) -> listener.accept(agent)));
    }
    public void removeAgentDisconnectedEventListener(Consumer<IntellectualAgent> listener){
        agentDisconnectedEventListeners.remove(listener);
    }
}
