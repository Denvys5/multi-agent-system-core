/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.tcp;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.TcpAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.service.OnEventFutureService;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.IMessageListener;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import com.denvys5.multi_agent_system.service.network.NetworkOnConnectExecutor;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLException;
import java.security.cert.CertificateException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class TcpAgentCommunicationInterface implements AgentCommunicationInterface {
    private IMultiAgentSystemConfiguration configController;
    private final OnEventFutureService networkOnConnectFutureService = new OnEventFutureService(1000);

    private AgentCommunicationData communicationData;
    private TcpAgentCommunicationInterfaceDataDto tcpCommData;

    private Optional<SslContext> sslCtx;
    private EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    private EventLoopGroup workerGroup = new NioEventLoopGroup();
    private SocketServerConnectionInitializer socketConnectionInitializer;
    private SocketServerConnectionHandler socketConnectionHandler;
    private final TcpTransmissionService tcpTransmissionService;

    public TcpAgentCommunicationInterface(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        tcpCommData = convertFromAbstract(communicationData.getCommsData());
        this.tcpTransmissionService = new TcpTransmissionService();
    }

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
    }

    @Override
    public void setMessageSerializationService(IMessageSerializationService serializationService) {
    }

    @Override
    public void setupConnectionProperties(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        tcpCommData = convertFromAbstract(communicationData.getCommsData());
    }

    public AgentCommunicationData getCommunicationData() {
        return communicationData;
    }

    @Override
    public void thenConnected(NetworkOnConnectExecutor networkOnConnectExecutor){
        CompletableFuture<Void> completableFuture = networkOnConnectFutureService.get();
        completableFuture.thenRun(() -> networkOnConnectExecutor.onConnect(this));
    }

    @Override
    public void init() {
        sslCtx = Optional.empty();
        if(tcpCommData.isSsl()){
            try {
                SelfSignedCertificate ssc = new SelfSignedCertificate();
                sslCtx = Optional.ofNullable(SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build());
            } catch (SSLException | CertificateException e) {
                if(configController.isDebug())
                    log.error("Failed to initialize SSL certificates", e);
            }
        }

        socketConnectionHandler = new SocketServerConnectionHandler();
        socketConnectionHandler.setTcpOnClientConnectToHostListener((channel) -> networkOnConnectFutureService.setFlag());

        socketConnectionInitializer = new SocketServerConnectionInitializer();
        socketConnectionInitializer.setSocketConnectionHandler(socketConnectionHandler);

        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup();
    }

    @Override
    public void connect() {
        if(tcpCommData.isSsl())
            sslCtx.ifPresent(sslContext -> socketConnectionInitializer.setSslCtx(sslContext));
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(socketConnectionInitializer);

            b.bind(tcpCommData.getPort()).sync();
        } catch (InterruptedException e) {
            if(configController.isDebug())
                log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void disconnect() {
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
        tcpTransmissionService.shutdown();
    }

    @Override
    public void subscribe(AgentCommunicationInterfaceDataDto commsData, IMessageListener listener) throws NetworkException {
        //TODO verify its empty
    }

    @Override
    public void subscribeToInput(IMessageListener listener) throws NetworkException {
        socketConnectionHandler.setReceiveMessageListener((ctx, msg)-> {
            try {
                listener.onMessageReceived(communicationData, msg);
            } catch (Exception e) {
                //TODO
                log.error("TODO WHERE IS THAT TRIGGERED?");
            }
        });
    }

    @Override
    public void subscribeToBroadcast(IMessageListener listener) throws NetworkException {

    }

    @Override
    public void unsubscribe() throws NetworkException {
        tcpTransmissionService.shutdown();
    }

    @Override
    public void sendMessageByCommsData(AgentCommunicationInterfaceDataDto commsData, String message) throws NetworkException {
        TcpAgentCommunicationInterfaceDataDto dataDto = convertFromAbstract(commsData);
        if(configController.isVerboseCommunication())
            log.info("Tcp publish to" + dataDto.getLocalhost() + ": " + message);
        tcpTransmissionService.sendMessageByCommsData(dataDto, message);
    }

    @Override
    public void broadcastMessageToAgents(String message) throws NetworkException {
        if(configController.isVerboseCommunication())
            log.info("Tcp broadcast: " + message);
        tcpTransmissionService.broadcastToKnownChannels(message);
    }

    private TcpAgentCommunicationInterfaceDataDto convertFromAbstract(AgentCommunicationInterfaceDataDto communicationData){
        return (TcpAgentCommunicationInterfaceDataDto) communicationData;
    }
}
