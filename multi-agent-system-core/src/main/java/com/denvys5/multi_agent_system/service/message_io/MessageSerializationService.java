/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;

public class MessageSerializationService implements IMessageSerializationService{
    private final Gson gson;

    public MessageSerializationService() {
        this.gson = new Gson();
    }

    @Override
    public String getStringFromPayload(byte[] payload){
        return new String(payload, StandardCharsets.UTF_8);
    }

    @Override
    public String getStringFromObject(Object message) {
        return gson.toJson(message);
    }

    @Override
    public byte[] getBytesFromObject(Object message) {
        return getStringFromObject(message).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public byte[] getBytesFromString(String payload){
        return payload.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String getStringFromBasicAgentMessage(BasicAgentMessageDto messageDto) {
        return gson.toJson(messageDto);
    }

    @Override
    public byte[] getBytesFromBasicAgentMessage(BasicAgentMessageDto messageDto) {
        return getStringFromBasicAgentMessage(messageDto).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public BasicAgentMessageDto getBasicAgentMessageFromString(String message) throws Exception{
        return gson.fromJson(message, BasicAgentMessageDto.class);
    }

    @Override
    public BasicAgentMessageDto getBasicAgentMessageFromBytes(byte[] message) throws Exception{
        return getBasicAgentMessageFromString(new String(message, StandardCharsets.UTF_8));
    }

    @Override
    public <T> T getObjectFromString(String string, Class<T> object) throws Exception{
        return gson.fromJson(string, object);
    }

    @Override
    public <T> T deserializeObject(Object object, Class<T> target) throws RuntimeException{
        return gson.fromJson(gson.toJson(object), target);
    }
}
