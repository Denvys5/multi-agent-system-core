/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.AgentTypeSetEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.AgentRequestIdTrackingService;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
public class MessageReceiveFactory implements IMessageReceiveFactory {
    private IMessageProcessor messageProcessor;
    private IMultiAgentSystemConfiguration configController;
    private AgentRequestIdTrackingService agentRequestIdTrackingService;
    private MessageReceiveEventService messageReceiveEventService;
    private IMessageSerializationService messageSerializationService;
    private IEnumerationHelper agentEnumerationHelper;

    private AgentEnumeration thisAgentEnumeration;
    private String thisAgentId;

    public BasicAgentMessage convertBasicAgentMessage(BasicAgentMessageDto message){
        return new BasicAgentMessage(
                message.getToAgent(),
                AgentTypeSetEnumeration.getTypeByValue(message.getBroadcastId()),
                message.getFromAgent(),
                message.getRequestID(),
                agentEnumerationHelper.getCommandById(thisAgentEnumeration, message.getCommandID()),
                message.getDataCommandID(),
                message.getDataVal(),
                message.getData()
        );
    }

    public void processIncomingMessage(AgentCommunicationData readFrom, String message){
        BasicAgentMessageDto basicAgentMessageDto = getValidMessageDto(message);
        if (basicAgentMessageDto == null)
            return;

        BasicAgentMessage basicAgentMessage = convertBasicAgentMessage(basicAgentMessageDto);

        if(!validateBasicAgentMessage(basicAgentMessage))
            return;

        if(!agentRequestIdTrackingService.addRequest(basicAgentMessage))
            return;

        processBasicAgentMessage(readFrom, basicAgentMessage);
    }


    public void processIncomingBroadcastMessage(AgentCommunicationData readFrom, String message){
        BasicAgentMessageDto basicAgentMessageDto = getValidMessageDto(message);
        if (basicAgentMessageDto == null)
            return;

        if(basicAgentMessageDto.getFromAgent().equals(thisAgentId))
            return;

        BasicAgentMessage basicAgentMessage = convertBasicAgentMessage(basicAgentMessageDto);

        if(!validateBasicAgentMessage(basicAgentMessage))
            return;

        if(!agentRequestIdTrackingService.addRequest(basicAgentMessage))
            return;

        processBroadcastAgentMessage(readFrom, basicAgentMessage);
    }

    private BasicAgentMessageDto getValidMessageDto(String message) {
        BasicAgentMessageDto basicAgentMessageDto;
        try {
            basicAgentMessageDto = messageSerializationService.getBasicAgentMessageFromString(message);
        } catch (Exception e) {
            if (configController.isDebug())
                log.error("GSON error in parsing message \"{}\"", message, e);
            return null;
        }

        if (!validateBasicAgentDtoMessage(basicAgentMessageDto))
            return null;
        return basicAgentMessageDto;
    }

    public void processBroadcastAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message){
        if(isPresentInPath(message)){
            messageReceiveEventService.getMessageReceivedEventListenerGroup().callEventListeners(readFrom, message);
            messageProcessor.processCommand(readFrom, message);
        }
    }

    public void processBasicAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message){
        if(isPresentInPath(message)){
            messageReceiveEventService.getMessageReceivedEventListenerGroup().callEventListeners(readFrom, message);
            messageProcessor.processCommand(readFrom, message);
        }else{
            messageProcessor.processMessageToOtherAgent(readFrom, message);
        }
    }

    public boolean isPresentInPath(BasicAgentMessage message){
        return message.getBroadcastId() == AgentTypeSetEnumeration.EVERYONE
                || message.getBroadcastId() == thisAgentEnumeration.getAgentTypeSet()
                || message.getToAgent().equals(thisAgentId);
    }
}
