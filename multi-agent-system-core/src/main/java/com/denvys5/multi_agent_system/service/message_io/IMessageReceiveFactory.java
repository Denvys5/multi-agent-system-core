/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.AgentTypeSetEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommandEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.AgentRequestIdTrackingService;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;

public interface IMessageReceiveFactory {
    void setMessageProcessor(IMessageProcessor messageProcessor);

    void setConfigController(IMultiAgentSystemConfiguration configController);

    void setMessageReceiveEventService(MessageReceiveEventService messageReceiveEventService);

    void setThisAgentEnumeration(AgentEnumeration thisAgentEnumeration);

    void setThisAgentId(String thisAgentId);

    void setAgentRequestIdTrackingService(AgentRequestIdTrackingService agentRequestIdTrackingService);

    void setMessageSerializationService(IMessageSerializationService messageSerializationService);

    void setAgentEnumerationHelper(IEnumerationHelper agentEnumerationHelper);

    default boolean validateBasicAgentDtoMessage(BasicAgentMessageDto message){
        if(message.getCommandID() == CommandEnumeration.DEFAULT.getCommandValue()){
            return false;
        }
        if(!validatePathParameter(message.getToAgent(), message.getBroadcastId())){
            return false;
        }
        if(!validatePathParameter(message.getFromAgent())){
            return false;
        }
        return true;
    }

    default boolean validatePathParameter(String fromAgent){
        if(fromAgent == null || fromAgent.isEmpty()){
            return false;
        }

        return true;
    }

    default boolean validatePathParameter(String toAgent, byte broadcastId){
        if((toAgent == null || toAgent.isEmpty()) && broadcastId == 0){
            return false;
        }
        if((toAgent != null && !toAgent.isEmpty()) && broadcastId != 0){
            return false;
        }

        return true;
    }

    default boolean validateBasicAgentMessage(BasicAgentMessage message){
        if(message.getBroadcastId() != AgentTypeSetEnumeration.NONE && validatePathParameter(message.getToAgent())){
            return false;
        }
        if(message.getBroadcastId() == AgentTypeSetEnumeration.NONE && !validatePathParameter(message.getToAgent())){
            return false;
        }
        if(message.getCommandID().hasDataval() && message.getDataVal() == 0){
            return false;
        }
        if(message.getCommandID().hasData() && message.getData() == null){
            return false;
        }
        if(!isPresentInPath(message)){
            //TODO if there is known agent, return true
            return false;
        }
        return true;
    }

    BasicAgentMessage convertBasicAgentMessage(BasicAgentMessageDto message);

    void processIncomingMessage(AgentCommunicationData readFrom, String message);

    void processIncomingBroadcastMessage(AgentCommunicationData readFrom, String message);

    void processBasicAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message);

    void processBroadcastAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message);

    boolean isPresentInPath(BasicAgentMessage message);
}
