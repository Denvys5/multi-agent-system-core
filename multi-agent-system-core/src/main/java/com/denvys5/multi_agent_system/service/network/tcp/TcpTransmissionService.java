/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.tcp;

import com.denvys5.multi_agent_system.config.dto.communication.TcpAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.service.network.NetworkException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class TcpTransmissionService {
    private final List<SocketClientConnector> knownChannels;

    public TcpTransmissionService() {
        knownChannels = new CopyOnWriteArrayList<>();
    }

    public Optional<SocketClientConnector> getCachedConnector(TcpAgentCommunicationInterfaceDataDto commsData){
        return knownChannels.stream()
                .filter(channel ->
                        channel.getHost().equals(commsData.getLocalhost()) && channel.getPort() == commsData.getPort() && channel.isSsl() == commsData.isSsl())
                .findAny();
    }

    public void sendMessageByCommsData(TcpAgentCommunicationInterfaceDataDto commsData, String message) throws NetworkException {
        Optional<SocketClientConnector> cachedOptional = getCachedConnector(commsData);
        if(cachedOptional.isPresent()){
            cachedOptional.get().writeToChannel(message);
        }else{
            SocketClientConnector socketClientConnector = new SocketClientConnector(commsData.getLocalhost(), commsData.getPort(), commsData.isSsl());
            socketClientConnector.writeOnConnect(message);
            knownChannels.add(socketClientConnector);
        }
    }

    public void broadcastToKnownChannels(String message){
        knownChannels.forEach(channel -> channel.writeToChannel(message));
    }

    public void shutdown(){
        knownChannels.forEach(SocketClientConnector::shutdown);
    }
}
