/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.denvys5.multi_agent_system.config.dto.communication.RosAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.TcpAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.UdpAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.MqttAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.model.message.*;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Setter
public class HandshakeFactory {
    private List<AgentCommunicationData> agentCommunicationDatas;

    public Optional<AgentCommunicationInterchangeDto> getInterchangeByCommsType(AgentCommunicationData agentCommunicationData){
        //TODO
        switch (agentCommunicationData.getCommsType()){
            case NONE:
                break;
            case UDP_BROADCAST:
                return Optional.of(
                        new UdpAgentCommunicationInterchangeDto(
                                (UdpAgentCommunicationInterfaceDataDto) agentCommunicationData.getCommsData()
                        )
                );
            case TCP_SOCKET:
                return Optional.of(
                        new TcpAgentCommunicationInterchangeDto(
                                (TcpAgentCommunicationInterfaceDataDto)agentCommunicationData.getCommsData()
                        )
                );
            case HTTP_REST_API:
                break;
            case MQTT_BROKER:
                return Optional.of(
                        new MqttAgentCommunicationInterchangeDto(
                                ((MqttAgentCommunicationInterfaceDataDto)agentCommunicationData.getCommsData())
                                        .getInputTopic()
                        )
                );
            case KAFKA_BROKER:
                break;
            case ROS_BROKER:
                return Optional.of(
                        new RosAgentCommunicationInterchangeDto(
                                ((RosAgentCommunicationInterfaceDataDto)agentCommunicationData.getCommsData())
                                        .getInputTopic()
                        )
                );
        }

        return Optional.empty();
    }


    public AgentCommunicationConfiguration getAgentCommunicationConfigurationForInterchange(){
        //TODO change some stuff, like remove passwords
        return new AgentCommunicationConfiguration(
                agentCommunicationDatas
                        .stream()
                        .map(AgentCommunicationData::toDto)
                        .collect(Collectors.toList())
        );
    }
}
