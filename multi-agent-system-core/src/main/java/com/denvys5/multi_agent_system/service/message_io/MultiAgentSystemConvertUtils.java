/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.multi_agent_system.config.dto.communication.*;
import com.denvys5.multi_agent_system.model.agent.*;
import com.denvys5.multi_agent_system.model.agent.dto.AgentCommunicationDataDto;
import com.denvys5.multi_agent_system.model.agent.dto.IntellectualAgentConfigDto;
import com.denvys5.multi_agent_system.model.agent.dto.MultiAgentNetworkConfigurationDto;
import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.denvys5.multi_agent_system.model.enumeration.AgentEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommunicationMethodEnumeration;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Setter
@Slf4j
public class MultiAgentSystemConvertUtils {
    private IMessageSerializationService messageSerializationService;

    public List<AgentCommunicationData> getAgentCommunicationDataListFromDtos(List<AgentCommunicationDataDto> dataDtos){
        List<AgentCommunicationData> result = new ArrayList<>();
        for (AgentCommunicationDataDto dto:dataDtos){
            AgentCommunicationData agentCommunicationData = getAgentCommunicationDataFromDto(dto);
            if(Objects.nonNull(agentCommunicationData))
                result.add(agentCommunicationData);
        }
        return result;
    }

    public List<AgentCommunicationData> getAgentCommunicationDataListFromDtos(AgentCommunicationConfiguration dataDtos){
        return getAgentCommunicationDataListFromDtos(dataDtos.getComms());
    }

    public boolean validateRosAgentCommunicationData(RosAgentCommunicationInterfaceDataDto data){
        if(data.getPort() == 0){
            return false;
        }
        if(Objects.isNull(data.getHost()) || data.getHost().isEmpty()){
            return false;
        }
        if(Objects.isNull(data.getInputTopic()) || data.getInputTopic().isEmpty()){
            return false;
        }

        return true;
    }

    public boolean validateMqttAgentCommunicationData(MqttAgentCommunicationInterfaceDataDto data){
        if(data.getPort() == 0){
            return false;
        }
        if(Objects.isNull(data.getHost()) || data.getHost().isEmpty()){
            return false;
        }
        if(Objects.isNull(data.getInputTopic()) || data.getInputTopic().isEmpty()){
            return false;
        }

        return true;
    }

    public boolean validateTcpAgentCommunicationData(TcpAgentCommunicationInterfaceDataDto data){
        if(data.getPort() == 0){
            return false;
        }
        if(Objects.isNull(data.getLocalhost()) || data.getLocalhost().isEmpty()){
            return false;
        }

        return true;
    }

    public boolean validateUdpAgentCommunicationData(UdpAgentCommunicationInterfaceDataDto data){
        if(data.getPort() == 0){
            return false;
        }
        if(Objects.isNull(data.getLocalhost()) || data.getLocalhost().isEmpty()){
            return false;
        }
        if(Objects.isNull(data.getBroadcastAddress()) || data.getBroadcastAddress().isEmpty()){
            return false;
        }

        return true;
    }

    public AgentCommunicationData getAgentCommunicationDataFromDto(AgentCommunicationDataDto dto){
        AgentCommunicationInterfaceDataDto commsData = null;
        CommunicationMethodEnumeration commsType = CommunicationMethodEnumeration.getCommsMethodFromId(dto.getCommsType());
        //TODO fill this
        boolean valid = false;
        try {
            switch (commsType){
                case NONE:
                    break;
                case UDP_BROADCAST:
                    try {
                        commsData = messageSerializationService.deserializeObject(dto.getCommsData(), UdpAgentCommunicationInterfaceDataDto.class);
                        valid = validateUdpAgentCommunicationData((UdpAgentCommunicationInterfaceDataDto) commsData);
                    }catch (Exception ignored){
                    }
                    break;
                case TCP_SOCKET:
                    try {
                        commsData = messageSerializationService.deserializeObject(dto.getCommsData(), TcpAgentCommunicationInterfaceDataDto.class);
                        valid = validateTcpAgentCommunicationData((TcpAgentCommunicationInterfaceDataDto) commsData);
                    }catch (Exception ignored){
                    }
                    break;
                case HTTP_REST_API:
                    break;
                case MQTT_BROKER:
                    try {
                        commsData = messageSerializationService.deserializeObject(dto.getCommsData(), MqttAgentCommunicationInterfaceDataDto.class);
                        valid = validateMqttAgentCommunicationData((MqttAgentCommunicationInterfaceDataDto) commsData);
                    }catch (Exception ignored){
                    }
                    break;
                case KAFKA_BROKER:
                    break;
                case ROS_BROKER:
                    try {
                        commsData = messageSerializationService.deserializeObject(dto.getCommsData(), RosAgentCommunicationInterfaceDataDto.class);
                        valid = validateRosAgentCommunicationData((RosAgentCommunicationInterfaceDataDto) commsData);
                    }catch (Exception ignored){
                    }
                    break;
            }
        }catch (Exception e){
            log.error("GSON error in parsing communication data \"{}\"", commsData, e);
        }
        if(valid)
            return new AgentCommunicationData(dto.getPriority(), commsType, commsData);
        else
            return null;
    }


    public MultiAgentNetworkConfiguration getMultiAgentNetworkConfigurationFromDto(MultiAgentNetworkConfigurationDto multiAgentNetworkConfigurationDto){
        List<IntellectualAgent> intellectualAgents = new ArrayList<>(multiAgentNetworkConfigurationDto.getAgents().size());
        for(IntellectualAgentConfigDto agentDto: multiAgentNetworkConfigurationDto.getAgents()){
            IntellectualAgent intellectualAgent = new IntellectualAgent();
            intellectualAgent.setAgentUuid(agentDto.getUuid());
            intellectualAgent.setAliasName(agentDto.getAlias());
            intellectualAgent.setAgentEnumeration(AgentEnumeration.getAgentEnumerationByTypeSetValue(agentDto.getType()));
            intellectualAgent.setSecureCommunication(agentDto.isSecureCommunication());
            intellectualAgent.setStatus(IEnumerationHelper.defaultStatusValue);
            intellectualAgent.setPublicEcdsaKeyB64(agentDto.getPublicEcdsaKey());

            List<AgentCommunicationData> communicationData = new ArrayList<>();
            for(AgentCommunicationDataDto agentCommunicationDataDto:agentDto.getCommsMethods()){
                AgentCommunicationData agentCommunicationData = getAgentCommunicationDataFromDto(agentCommunicationDataDto);
                if(Objects.nonNull(agentCommunicationData))
                    communicationData.add(agentCommunicationData);
            }
            intellectualAgent.setCommsMethods(communicationData);

            intellectualAgents.add(intellectualAgent);
        }
        return new MultiAgentNetworkConfiguration(intellectualAgents);
    }

    public MultiAgentNetworkConfigurationDto getMultiAgentNetworkConfigurationDto(MultiAgentNetworkConfiguration multiAgentNetworkConfiguration){
        return new MultiAgentNetworkConfigurationDto(
                multiAgentNetworkConfiguration.getAgents()
                        .stream()
                        .map(IntellectualAgentConfigDto::new)
                        .collect(Collectors.toList()));
    }
}
