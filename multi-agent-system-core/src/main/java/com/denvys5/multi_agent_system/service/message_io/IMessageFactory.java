/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.crypto.RandomUtil;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.CommandEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;

public interface IMessageFactory {
    default long getUniqueRequestId(){
        return RandomUtil.uniqueLong();
    }

    void setHandshakeFactory(HandshakeFactory handshakeFactory);

    void setThisDeviceUuid(String thisDeviceUuid);

    void setThisDeviceAlias(String thisDeviceAlias);

    void setThisDeviceType(byte thisDeviceType);

    void setThisDeviceSecureCommunication(boolean secureCommunication);

    void setCryptoService(ICryptoService cryptoService);

    void setDataMessageFactory(IDataMessageFactory dataMessageFactory);

    void setSerializationService(IMessageSerializationService serializationService);

    BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId, short dataCommandId, short dataval, Object data);

    default BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId, short dataval, Object data){
        return getMessage(commandEnumeration, requestId, (short) 0, dataval, data);
    }

    default BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId, short dataval){
        return getMessage(commandEnumeration, requestId, dataval, null);
    }

    default BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId, Object data){
        return getMessage(commandEnumeration, requestId, (short) 0, data);
    }

    default BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId){
        return getMessage(commandEnumeration, requestId, (short) 0, null);
    }

    BasicAgentMessageDto getPingRequestMessage();

    BasicAgentMessageDto getPingResponseMessage(long requestId);

    BasicAgentMessageDto getStatusRequestMessage();

    BasicAgentMessageDto getStatusResponseMessage(long requestId, short result);

    BasicAgentMessageDto getAgentDiscoveryMessage(AgentCommunicationData agentCommunicationData);

    BasicAgentMessageDto getHandshakeRequestMessage(AgentCommunicationData agentCommunicationData);

    BasicAgentMessageDto getHandshakeResponseMessage(long requestId, short result);

    BasicAgentMessageDto getCommunicationInterchangeRequestMessage();

    BasicAgentMessageDto getCommunicationInterchangeResponseMessage(long requestId, Object configuration);

    BasicAgentMessageDto getDataMessage(long requestId, short dataCommandId, short dataval, Object data);

    BasicAgentMessageDto getSecuredDataMessage(long requestId, short dataCommandId, short dataval, IntellectualAgent toAgent, Object data);

    Object encryptData(IntellectualAgent toAgent, Object data);
}
