/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.AgentTypeSetEnumeration;
import com.denvys5.multi_agent_system.model.enumeration.CommandEnumeration;
import com.denvys5.multi_agent_system.model.message.AgentCommunicationInterchangeDto;
import com.denvys5.multi_agent_system.model.message.AgentSecuredDataDto;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import lombok.Setter;

import java.util.Objects;
import java.util.Optional;


@Setter
public class AgentMessageFactory implements IMessageFactory {
    protected String thisDeviceUuid;
    protected String thisDeviceAlias;
    protected byte thisDeviceType;
    protected boolean thisDeviceSecureCommunication;

    protected HandshakeFactory handshakeFactory;
    protected ICryptoService cryptoService;
    protected IDataMessageFactory dataMessageFactory;
    protected IMessageSerializationService serializationService;

    @Override
    public BasicAgentMessageDto getMessage(CommandEnumeration commandEnumeration, long requestId, short dataCommandId, short dataval, Object data){
        BasicAgentMessageDto messageDto = null;
        switch (commandEnumeration){
            case DEFAULT:
            case HANDSHAKE_RESPONSE:
            case HANDSHAKE:
            case AGENT_DISCOVERY_BROADCAST:
            default:
                //NOT IMPLEMENTED IN THIS CLASS ON PURPOSE
                break;
            case PING: messageDto = getPingRequestMessage(); break;
            case PING_RESPONSE: messageDto = getPingResponseMessage(requestId); break;
            case STATUS: messageDto = getStatusRequestMessage(); break;
            case STATUS_RESPONSE: messageDto = getStatusResponseMessage(requestId, dataval); break;

            case COMMUNICATION_INTERCHANGE: messageDto = getCommunicationInterchangeRequestMessage(); break;
            case COMMUNICATION_INTERCHANGE_RESPONSE: messageDto = getCommunicationInterchangeResponseMessage(requestId, data); break;
            case DATA_COMMAND: messageDto = getDataMessage(requestId, dataCommandId, dataval, data); break;
        }
        return messageDto;
    }

    @Override
    public BasicAgentMessageDto getPingRequestMessage(){
        return new BasicAgentMessageDto()
                .setRequestID(getUniqueRequestId())
                .setCommandID(CommandEnumeration.PING.getCommandValue())
                .setFromAgent(thisDeviceUuid);
    }

    @Override
    public BasicAgentMessageDto getPingResponseMessage(long requestId){
        return new BasicAgentMessageDto()
                .setRequestID(requestId)
                .setCommandID(CommandEnumeration.PING_RESPONSE.getCommandValue())
                .setFromAgent(thisDeviceUuid);
    }

    @Override
    public BasicAgentMessageDto getStatusRequestMessage(){
        return new BasicAgentMessageDto()
                .setRequestID(getUniqueRequestId())
                .setCommandID(CommandEnumeration.STATUS.getCommandValue())
                .setFromAgent(thisDeviceUuid);
    }

    @Override
    public BasicAgentMessageDto getStatusResponseMessage(long requestId, short result){
        return new BasicAgentMessageDto()
                .setRequestID(requestId)
                .setCommandID(CommandEnumeration.STATUS_RESPONSE.getCommandValue())
                .setFromAgent(thisDeviceUuid)
                .setDataVal(result);
    }

    @Override
    public BasicAgentMessageDto getAgentDiscoveryMessage(AgentCommunicationData agentCommunicationData) {
        Optional<AgentCommunicationInterchangeDto> agentDiscoveryBroadcastData = handshakeFactory.getInterchangeByCommsType(agentCommunicationData);
        agentDiscoveryBroadcastData.ifPresent(data ->
                data.setAgentAlias(thisDeviceAlias)
                        .setType(thisDeviceType)
                        .setSecureCommunication(thisDeviceSecureCommunication)
                        .setPublicEcdhKey(cryptoService.getPublicEcdhKeyB64())
                        .setPublicEcdsaKey(cryptoService.getPublicEcdsaKeyB64()));
        BasicAgentMessageDto message = new BasicAgentMessageDto()
                .setRequestID(getUniqueRequestId())
                .setCommandID(CommandEnumeration.AGENT_DISCOVERY_BROADCAST.getCommandValue())
                .setFromAgent(thisDeviceUuid)
                .setBroadcastId(AgentTypeSetEnumeration.EVERYONE.getTypeValue());
        agentDiscoveryBroadcastData.ifPresent(message::setData);

        return message;
    }

    @Override
    public BasicAgentMessageDto getHandshakeRequestMessage(AgentCommunicationData agentCommunicationData) {
        Optional<AgentCommunicationInterchangeDto> agentDiscoveryBroadcastData = handshakeFactory.getInterchangeByCommsType(agentCommunicationData);
        agentDiscoveryBroadcastData.ifPresent(data ->
                data.setAgentAlias(thisDeviceAlias)
                        .setType(thisDeviceType)
                        .setSecureCommunication(thisDeviceSecureCommunication)
                        .setPublicEcdhKey(cryptoService.getPublicEcdhKeyB64())
                        .setPublicEcdsaKey(cryptoService.getPublicEcdsaKeyB64()));
        BasicAgentMessageDto message = new BasicAgentMessageDto()
                .setRequestID(getUniqueRequestId())
                .setCommandID(CommandEnumeration.HANDSHAKE.getCommandValue())
                .setFromAgent(thisDeviceUuid);
        agentDiscoveryBroadcastData.ifPresent(message::setData);

        return message;
    }

    @Override
    public BasicAgentMessageDto getHandshakeResponseMessage(long requestId, short result) {
        return new BasicAgentMessageDto()
                .setRequestID(requestId)
                .setCommandID(CommandEnumeration.HANDSHAKE_RESPONSE.getCommandValue())
                .setFromAgent(thisDeviceUuid)
                .setDataVal(result);
    }

    @Override
    public BasicAgentMessageDto getCommunicationInterchangeRequestMessage() {
        return new BasicAgentMessageDto()
                .setRequestID(getUniqueRequestId())
                .setCommandID(CommandEnumeration.COMMUNICATION_INTERCHANGE.getCommandValue())
                .setFromAgent(thisDeviceUuid);
    }

    @Override
    public BasicAgentMessageDto getCommunicationInterchangeResponseMessage(long requestId, Object configuration) {
        return new BasicAgentMessageDto()
                .setRequestID(requestId)
                .setCommandID(CommandEnumeration.COMMUNICATION_INTERCHANGE_RESPONSE.getCommandValue())
                .setFromAgent(thisDeviceUuid)
                .setData(configuration);
    }

    @Override
    public BasicAgentMessageDto getDataMessage(long requestId, short dataCommandId, short dataval, Object data) {
        return dataMessageFactory.getMessage(requestId, dataCommandId, dataval, data);
    }

    @Override
    public BasicAgentMessageDto getSecuredDataMessage(long requestId, short dataCommandId, short dataval, IntellectualAgent toAgent, Object data) {
        return dataMessageFactory.getMessage(requestId, dataCommandId, dataval, encryptData(toAgent, data));
    }

    @Override
    public Object encryptData(IntellectualAgent toAgent, Object data) {
        if(Objects.isNull(data)){
            return null;
        }
        if(Objects.isNull(toAgent.getSecretEcdhKey())){
            return data;
        }
        String responseData = data instanceof String ? (String) data : serializationService.getStringFromObject(data);
        String encryptedData = cryptoService.encryptString(responseData, toAgent.getSecretEcdhKey());
        return new AgentSecuredDataDto(cryptoService.signString(encryptedData), encryptedData);
    }
}
