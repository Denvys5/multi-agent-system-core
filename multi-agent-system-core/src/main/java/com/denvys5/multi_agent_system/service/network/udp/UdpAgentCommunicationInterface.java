/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.udp;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.UdpAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.service.OnEventFutureService;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.IMessageListener;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import com.denvys5.multi_agent_system.service.network.NetworkOnConnectExecutor;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class UdpAgentCommunicationInterface implements AgentCommunicationInterface {
    private final OnEventFutureService networkOnConnectFutureService = new OnEventFutureService(1000);

    private IMultiAgentSystemConfiguration configController;

    private AgentCommunicationData communicationData;
    private UdpAgentCommunicationInterfaceDataDto udpCommData;

    private EventLoopGroup group;
    private UdpConnectionHandler socketConnectionHandler;
    private final UdpConnector udpConnector;

    public UdpAgentCommunicationInterface(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        udpCommData = convertFromAbstract(communicationData.getCommsData());
        this.udpConnector = new UdpConnector();
    }

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
    }

    @Override
    public void setMessageSerializationService(IMessageSerializationService serializationService) {
    }

    @Override
    public void setupConnectionProperties(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        udpCommData = convertFromAbstract(communicationData.getCommsData());
    }

    public AgentCommunicationData getCommunicationData() {
        return communicationData;
    }

    @Override
    public void thenConnected(NetworkOnConnectExecutor networkOnConnectExecutor){
        CompletableFuture<Void> completableFuture = networkOnConnectFutureService.get();
        completableFuture.thenRun(() -> networkOnConnectExecutor.onConnect(this));
    }

    @Override
    public void init() {
        socketConnectionHandler = new UdpConnectionHandler();
        group = new NioEventLoopGroup();
    }

    @Override
    public void connect() {
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(socketConnectionHandler);

            b.bind(udpCommData.getPort()).sync();
            networkOnConnectFutureService.setFlag();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void disconnect() {
        group.shutdownGracefully();
        udpConnector.shutdown();
    }

    @Override
    public void subscribe(AgentCommunicationInterfaceDataDto commsData, IMessageListener listener) throws NetworkException {
        //TODO verify its empty
    }

    @Override
    public void subscribeToInput(IMessageListener listener) throws NetworkException {
        socketConnectionHandler.setListener((ctx, msg)-> {
            try {
                listener.onMessageReceived(communicationData, msg.content().toString(StandardCharsets.UTF_8));
            } catch (Exception e) {
                //TODO
                log.error("TODO WHERE IS THAT TRIGGERED?");
            }
        });
    }

    @Override
    public void subscribeToBroadcast(IMessageListener listener) throws NetworkException {

    }

    @Override
    public void unsubscribe() throws NetworkException {
        udpConnector.shutdown();
    }

    @Override
    public void sendMessageByCommsData(AgentCommunicationInterfaceDataDto commsData, String message) throws NetworkException {
        broadcastMessageToAgents(message);
    }

    @Override
    public void broadcastMessageToAgents(String message) throws NetworkException {
        broadcast(udpCommData.getBroadcastAddress(), udpCommData.getPort(), message);
    }

    private void broadcast(String address, int port, String message){
        if(configController.isVerboseCommunication())
            log.info("Udp publish to" + address + ": " + message);
        udpConnector.broadcast(address, port, message);
    }

    private UdpAgentCommunicationInterfaceDataDto convertFromAbstract(AgentCommunicationInterfaceDataDto communicationData){
        return (UdpAgentCommunicationInterfaceDataDto) communicationData;
    }
}
