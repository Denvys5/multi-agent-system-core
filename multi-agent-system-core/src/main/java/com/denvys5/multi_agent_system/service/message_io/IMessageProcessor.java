/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.controller.AgentStatusController;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.controller.MultiAgentNetworkController;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.enumeration.CommandEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;

public interface IMessageProcessor {
    void setConfigController(IMultiAgentSystemConfiguration configController);

    void setHandshakeFactory(HandshakeFactory handshakeFactory);

    void setMultiAgentNetworkController(MultiAgentNetworkController multiAgentNetworkController);

    void setMessageReceiveEventService(MessageReceiveEventService messageReceiveEventService);

    void setAgentMessageFactory(IMessageFactory agentMessageFactory);

    void setStatusController(AgentStatusController statusController);

    void setSerializationService(IMessageSerializationService serializationService);

    void setCryptoService(ICryptoService cryptoService);

    void setDataCommandProcessor(IDataCommandProcessor dataCommandProcessor);

    void setAgentEnumerationHelper(IEnumerationHelper agentEnumerationHelper);

    void setMultiAgentSystemConvertUtils(MultiAgentSystemConvertUtils multiAgentSystemConvertUtils);

    default void processCommand(AgentCommunicationData readFrom, BasicAgentMessage message){
        switch (message.getCommandID()){
            case DEFAULT: defaultMessageProcessor(readFrom, message); break;
            case PING: pingRequestMessageProcessor(readFrom, message); break;
            case PING_RESPONSE: pingResponseMessageProcessor(readFrom, message); break;
            case STATUS: statusRequestMessageProcessor(readFrom, message); break;
            case STATUS_RESPONSE: statusResponseMessageProcessor(readFrom, message); break;
            case AGENT_DISCOVERY_BROADCAST: agentDiscoveryBroadcastMessageProcessor(readFrom, message); break;
            case HANDSHAKE: handshakeRequestMessageProcessor(readFrom, message); break;
            case HANDSHAKE_RESPONSE: handshakeResponseMessageProcessor(readFrom, message); break;
            case COMMUNICATION_INTERCHANGE: communicationInterchangeMessageProcessor(readFrom, message); break;
            case COMMUNICATION_INTERCHANGE_RESPONSE: communicationInterchangeResponseMessageProcessor(readFrom, message); break;
            case DATA_COMMAND: dataMessageProcessor(readFrom, message); break;
        }
        logAgentMessage(readFrom, message);
    }

    void defaultMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void processMessageToOtherAgent(AgentCommunicationData readFrom, BasicAgentMessage message);

    void logAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message);

    void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo, short dataval, Object data);

    default void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo, short dataval) {
        sendMessage(commandEnumeration, requestId, messageTo, dataval, null);
    }

    default void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo, Object data) {
        sendMessage(commandEnumeration, requestId, messageTo, (short) 0, data);
    }

    default void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo) {
        sendMessage(commandEnumeration, requestId, messageTo, (short) 0, null);
    }

    void pingRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void pingResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void statusRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void statusResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void agentDiscoveryBroadcastMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void handshakeRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void handshakeResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void communicationInterchangeMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void communicationInterchangeResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);

    void dataMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message);
}
