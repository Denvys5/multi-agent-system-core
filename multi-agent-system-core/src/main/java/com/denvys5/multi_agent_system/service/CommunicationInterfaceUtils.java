/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service;

import com.denvys5.multi_agent_system.config.dto.communication.*;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.message.*;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.mqtt.MqttAgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.none.NoCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.ros.RosAgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.tcp.TcpAgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.udp.UdpAgentCommunicationInterface;
import lombok.Setter;

@Setter
public class CommunicationInterfaceUtils {
    private IMessageSerializationService serializationService;

    public AgentCommunicationData getAgentCommunicationDataFromInterchangeMessage(AgentCommunicationData readFrom, BasicAgentMessage message){
        //TODO
        AgentCommunicationData targetCommunicationData = null;
        switch (readFrom.getCommsType()) {
            case NONE:
                break;
            case UDP_BROADCAST:
                try {
                    UdpAgentCommunicationInterfaceDataDto agentCommunicationInterchangeDto = serializationService.deserializeObject(message.getData(), UdpAgentCommunicationInterfaceDataDto.class);
                    UdpAgentCommunicationInterfaceDataDto communicationDataDto = new UdpAgentCommunicationInterfaceDataDto(agentCommunicationInterchangeDto.getLocalhost(), agentCommunicationInterchangeDto.getBroadcastAddress(), agentCommunicationInterchangeDto.getPort());
                    targetCommunicationData = new AgentCommunicationData((byte) 1, readFrom.getCommsType(), communicationDataDto);
                } catch (Exception ignored) {
                }
                break;
            case TCP_SOCKET:
                try {
                    TcpAgentCommunicationInterchangeDto agentCommunicationInterchangeDto = serializationService.deserializeObject(message.getData(), TcpAgentCommunicationInterchangeDto.class);
                    TcpAgentCommunicationInterfaceDataDto communicationDataDto = new TcpAgentCommunicationInterfaceDataDto(agentCommunicationInterchangeDto.getLocalhost(), agentCommunicationInterchangeDto.getPort(), agentCommunicationInterchangeDto.isSsl());
                    targetCommunicationData = new AgentCommunicationData((byte) 1, readFrom.getCommsType(), communicationDataDto);
                } catch (Exception ignored) {
                }
                break;
            case HTTP_REST_API:
                break;
            case MQTT_BROKER:
                try {
                    MqttAgentCommunicationInterchangeDto agentCommunicationInterchangeDto = serializationService.deserializeObject(message.getData(), MqttAgentCommunicationInterchangeDto.class);
                    MqttAgentCommunicationInterfaceDataDto oldCommsData = (MqttAgentCommunicationInterfaceDataDto) readFrom.getCommsData();
                    MqttAgentCommunicationInterfaceDataDto communicationDataDto = new MqttAgentCommunicationInterfaceDataDto(oldCommsData.getHost(), oldCommsData.getPort(), false,"", "", agentCommunicationInterchangeDto.getInputTopic(),  oldCommsData.getBroadcastTopic());
                    targetCommunicationData = new AgentCommunicationData((byte) 1, readFrom.getCommsType(), communicationDataDto);
                } catch (Exception ignored) {
                }
                break;
            case KAFKA_BROKER:
                break;
            case ROS_BROKER:
                try {
                    RosAgentCommunicationInterchangeDto agentCommunicationInterchangeDto = serializationService.deserializeObject(message.getData(), RosAgentCommunicationInterchangeDto.class);
                    RosAgentCommunicationInterfaceDataDto oldCommsData = (RosAgentCommunicationInterfaceDataDto) readFrom.getCommsData();
                    RosAgentCommunicationInterfaceDataDto communicationDataDto = new RosAgentCommunicationInterfaceDataDto(oldCommsData.getHost(), oldCommsData.getPort(), "", "", agentCommunicationInterchangeDto.getInputTopic(), oldCommsData.getBroadcastTopic());
                    targetCommunicationData = new AgentCommunicationData((byte) 1, readFrom.getCommsType(), communicationDataDto);
                } catch (Exception ignored) {
                }
                break;
        }
        return targetCommunicationData;
    }

    public AgentCommunicationInterfaceDataDto getAgentCommunicationDataDtoFromHandshake(AgentCommunicationData readFrom, BasicAgentMessage message){
        AgentCommunicationInterfaceDataDto agentCommunicationInterfaceDataDto = null;
        //TODO
        switch (readFrom.getCommsType()){
            case NONE:
                break;
            case UDP_BROADCAST:
                try {
                    UdpAgentCommunicationInterchangeDto udpInput = serializationService.deserializeObject(message.getData(), UdpAgentCommunicationInterchangeDto.class);
                    agentCommunicationInterfaceDataDto = new UdpAgentCommunicationInterfaceDataDto(udpInput.getLocalhost(), udpInput.getBroadcastAddress(), udpInput.getPort());
                }catch (Exception ignored){
                }
                break;
            case TCP_SOCKET:
                try {
                    TcpAgentCommunicationInterchangeDto tcpInput = serializationService.deserializeObject(message.getData(), TcpAgentCommunicationInterchangeDto.class);
                    agentCommunicationInterfaceDataDto = new TcpAgentCommunicationInterfaceDataDto(tcpInput.getLocalhost(), tcpInput.getPort(), tcpInput.isSsl());
                }catch (Exception ignored){
                }
                break;
            case HTTP_REST_API:
                break;
            case MQTT_BROKER:
                try {
                    MqttAgentCommunicationInterfaceDataDto actualCommDataDto = ((MqttAgentCommunicationInterfaceDataDto)readFrom.getCommsData());
                    String inputTopic = serializationService.deserializeObject(message.getData(), MqttAgentCommunicationInterchangeDto.class).getInputTopic();
                    agentCommunicationInterfaceDataDto = new MqttAgentCommunicationInterfaceDataDto(actualCommDataDto.getHost(), actualCommDataDto.getPort(), false, "", "", inputTopic,  actualCommDataDto.getBroadcastTopic());
                }catch (Exception ignored){
                }
                break;
            case KAFKA_BROKER:
                break;
            case ROS_BROKER:
                try {
                    RosAgentCommunicationInterfaceDataDto actualCommDataDto = ((RosAgentCommunicationInterfaceDataDto)readFrom.getCommsData());
                    String inputTopic = serializationService.deserializeObject(message.getData(), RosAgentCommunicationInterchangeDto.class).getInputTopic();
                    agentCommunicationInterfaceDataDto = new RosAgentCommunicationInterfaceDataDto(actualCommDataDto.getHost(), actualCommDataDto.getPort(), "", "", inputTopic, actualCommDataDto.getBroadcastTopic());
                }catch (Exception ignored){
                }
                break;
        }
        return agentCommunicationInterfaceDataDto;
    }

    public AgentCommunicationInterface getAgentCommunicationInterfaceFromCommsData(AgentCommunicationData comms){
        AgentCommunicationInterface newComm = null;
        //TODO
        switch (comms.getCommsType()){
            case NONE:
                newComm = new NoCommunicationInterface();
                break;
            case UDP_BROADCAST:
                newComm = new UdpAgentCommunicationInterface(comms);
                break;
            case TCP_SOCKET:
                newComm = new TcpAgentCommunicationInterface(comms);
                break;
            case HTTP_REST_API:
                break;
            case MQTT_BROKER:
                newComm = new MqttAgentCommunicationInterface(comms);
                break;
            case KAFKA_BROKER:
                break;
            case ROS_BROKER:
                newComm = new RosAgentCommunicationInterface(comms);
                break;
        }
        return newComm;
    }
}
