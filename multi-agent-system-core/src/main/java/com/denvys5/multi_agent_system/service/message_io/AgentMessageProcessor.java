/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.crypto.CryptographicException;
import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.controller.AgentStatusController;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.controller.MultiAgentNetworkController;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.*;
import com.denvys5.multi_agent_system.model.message.AgentSecuredDataDto;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.message_io.event.MessageReceiveEventService;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import com.google.gson.JsonSyntaxException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Setter
@Slf4j
public class AgentMessageProcessor implements IMessageProcessor {
    protected MultiAgentNetworkController multiAgentNetworkController;
    protected IMessageFactory agentMessageFactory;
    protected AgentStatusController statusController;
    protected MessageReceiveEventService messageReceiveEventService;
    protected HandshakeFactory handshakeFactory;
    protected IMultiAgentSystemConfiguration configController;
    protected ICryptoService cryptoService;
    protected IMessageSerializationService serializationService;
    protected IDataCommandProcessor dataCommandProcessor;
    protected IEnumerationHelper agentEnumerationHelper;
    protected MultiAgentSystemConvertUtils multiAgentSystemConvertUtils;

    @Override
    public void defaultMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message) {
        log.warn("Unknown message {}", message);
    }

    @Override
    public void processMessageToOtherAgent(AgentCommunicationData readFrom, BasicAgentMessage message) {
        //TODO
        multiAgentNetworkController.sendMessageToAgentById(message.getToAgent(), message.toDto());
    }

    @Override
    public void logAgentMessage(AgentCommunicationData readFrom, BasicAgentMessage message) {
        if(message.getBroadcastId() == AgentTypeSetEnumeration.NONE){
            multiAgentNetworkController.getAgentById(message.getFromAgent()).ifPresent(intellectualAgent -> {
                intellectualAgent.setLastMessageReceived(System.currentTimeMillis());
                //Added fix to handle synchronisation with AgentDiscoveryController
                if(!intellectualAgent.isOnline() && message.getCommandID() != CommandEnumeration.HANDSHAKE)
                    intellectualAgent.setOnline(true);
            });
        }
    }

    @Override
    public void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo, short dataval, Object data){
        BasicAgentMessageDto messageDto = agentMessageFactory.getMessage(commandEnumeration, requestId, dataval, data);

        try {
            if(Objects.nonNull(messageDto)){
                multiAgentNetworkController.sendMessageToAgentById(messageTo, messageDto);
            }
        } catch (NetworkException e) {
            if(configController.isDebug())
                log.error(e.getMessage(), e);
        }
    }

    @Override
    public void pingRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);

        //Ping response
        sendMessage(CommandEnumeration.PING_RESPONSE, message.getRequestID(), message.getFromAgent());
    }

    @Override
    public void pingResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
    }

    @Override
    public void statusRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        //Status response
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);

        sendMessage(CommandEnumeration.STATUS_RESPONSE, message.getRequestID(), message.getFromAgent(), statusController.getCurrentStatus().getStatusValue());
    }

    @Override
    public void statusResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        //status handling
        multiAgentNetworkController.getAgentById(message.getFromAgent()).ifPresent(intellectualAgent -> {
            intellectualAgent.setStatus(
                    agentEnumerationHelper.getStatusEnumerationOfDeviceByValue(intellectualAgent.getAgentEnumeration(), message.getDataVal()));
        });

        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
    }

    @Override
    public void agentDiscoveryBroadcastMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message) {
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
    }

    @Override
    public void handshakeRequestMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
    }

    @Override
    public void handshakeResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message){
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
    }

    @Override
    public void communicationInterchangeMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message) {
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);

        sendMessage(CommandEnumeration.COMMUNICATION_INTERCHANGE_RESPONSE, message.getRequestID(), message.getFromAgent(), handshakeFactory.getAgentCommunicationConfigurationForInterchange());
    }

    @Override
    public void communicationInterchangeResponseMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message) {
        messageReceiveEventService.getListenerGroupByCommand(message.getCommandID()).callEventListeners(readFrom, message);
        AgentCommunicationConfiguration agentCommunicationConfiguration = null;
        try {
            agentCommunicationConfiguration = serializationService.deserializeObject(message.getData(), AgentCommunicationConfiguration.class);
        }catch (Exception ignored){
        }
        if(Objects.nonNull(agentCommunicationConfiguration)){
            List<AgentCommunicationData> communicationDataList = multiAgentSystemConvertUtils.getAgentCommunicationDataListFromDtos(agentCommunicationConfiguration);
            multiAgentNetworkController.getAgentById(message.getFromAgent()).ifPresent(agent->{
                List<AgentCommunicationData> agentsCommunicationDataList = agent.getCommsMethods();

                //TODO kinda improve this (?)
                agentsCommunicationDataList.stream().filter(data ->
                        communicationDataList.stream()
                                .anyMatch(oldData ->
                                        oldData.getCommsType() == data.getCommsType()))
                        .forEach(data->{
                            AgentCommunicationData newData = communicationDataList.stream()
                                    .filter(oldData ->
                                            oldData.getCommsType() == data.getCommsType())
                                    .findAny().get();
                            data.setPriority(newData.getPriority());
                        });

                communicationDataList.stream()
                        .filter(data ->
                                agentsCommunicationDataList.stream()
                                        .noneMatch(oldData ->
                                                oldData.getCommsType() == data.getCommsType()))
                        .forEach(agentsCommunicationDataList::add);
            });
        }
    }

    @Override
    public void dataMessageProcessor(AgentCommunicationData readFrom, BasicAgentMessage message) {
        if(Objects.isNull(message.getData())){
            dataCommandProcessor.processCommand(readFrom, message);
            return;
        }

        if(!configController.isSecureCommunication()){
            dataCommandProcessor.processCommand(readFrom, message);
            return;
        }

        Optional<IntellectualAgent> agentOptional = multiAgentNetworkController.getAgentById(message.getFromAgent());

        if(!agentOptional.map(IntellectualAgent::isSecureCommunication).orElse(false)){
            dataCommandProcessor.processCommand(readFrom, message);
            return;
        }

        IntellectualAgent agent = agentOptional.get();
        try {
            AgentSecuredDataDto encryptedData = serializationService.deserializeObject(message.getData(), AgentSecuredDataDto.class);

            if(StringUtils.isEmpty(encryptedData.getSignature()) || StringUtils.isEmpty(encryptedData.getData())){
                log.error("Unable to process secure message {}", message);
                dataCommandProcessor.processCommand(readFrom, message);
                return;
            }

            if(Objects.isNull(agent.getSecretEcdhKey()) || Objects.isNull(agent.getPublicEcdsaKey())){
                log.error("Unable to process secure message as agent's keys are missing {}", message);
                return;
            }

            if(!cryptoService.verifyString(encryptedData.getData(), encryptedData.getSignature(), agent.getPublicEcdsaKey())){
                log.warn("Unable to verify encryption on the message {}", message);
                return;
            }

            String data = cryptoService.decryptString(encryptedData.getData(), agent.getSecretEcdhKey());
            dataCommandProcessor.processSecuredCommand(readFrom, agent, message, data);
        }catch (JsonSyntaxException e){
            if(configController.isDebug())
                log.error("Unable to decode secure message {}", message, e);
            dataCommandProcessor.processCommand(readFrom, message);
        }catch (CryptographicException e){
            log.error("Unable to verify secure message {}", message, e);
        }catch (Exception e){
            log.error("Unable to process secure message {}", message, e);
        }
    }

}
