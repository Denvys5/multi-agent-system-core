/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.message_io;

import com.denvys5.crypto.ICryptoService;
import com.denvys5.multi_agent_system.controller.AgentStatusController;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.controller.MultiAgentNetworkController;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.model.agent.IntellectualAgent;
import com.denvys5.multi_agent_system.model.enumeration.CommandEnumeration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessageDto;
import com.denvys5.multi_agent_system.service.IEnumerationHelper;
import com.denvys5.multi_agent_system.service.IShutdownService;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@Setter
public class SimpleDataMessageProcessor implements IDataCommandProcessor{
    protected MultiAgentNetworkController multiAgentNetworkController;
    protected IMessageFactory agentMessageFactory;
    protected AgentStatusController statusController;
    protected HandshakeFactory handshakeFactory;
    protected IMultiAgentSystemConfiguration configController;
    protected IShutdownService shutdownService;
    protected ICryptoService cryptoService;
    protected IMessageSerializationService serializationService;
    protected IEnumerationHelper agentEnumerationHelper;

    @Override
    public void processCommand(AgentCommunicationData readFrom, BasicAgentMessage message) {
        log.info("PROCESSING MESSAGE {}", message);
    }

    @Override
    public void processSecuredCommand(AgentCommunicationData readFrom, IntellectualAgent fromAgent, BasicAgentMessage message, String data){
        log.info("PROCESSING SECURE MESSAGE {}, {}", message, data);
    }

    @Override
    public void sendMessage(CommandEnumeration commandEnumeration, long requestId, String messageTo, short dataCommandId, short dataval, Object data){
        BasicAgentMessageDto messageDto = agentMessageFactory.getMessage(commandEnumeration, requestId, dataCommandId, dataval, data);

        try {
            if(Objects.nonNull(messageDto)){
                multiAgentNetworkController.sendMessageToAgentById(messageTo, messageDto);
            }
        } catch (NetworkException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void sendSecureDataMessage(long requestId, IntellectualAgent toAgent, short dataCommandId, short dataval, Object data){
        BasicAgentMessageDto messageDto = agentMessageFactory.getSecuredDataMessage(requestId, dataCommandId, dataval, toAgent, data);

        try {
            if(Objects.nonNull(messageDto)){
                multiAgentNetworkController.sendMessageToAgent(toAgent, messageDto);
            }
        } catch (NetworkException e) {
            log.error(e.getMessage(), e);
        }
    }
}
