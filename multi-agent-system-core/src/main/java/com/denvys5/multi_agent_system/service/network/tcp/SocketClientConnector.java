/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
public class SocketClientConnector {
    //TODO remove prints
    private final String host;
    private final int port;
    private final boolean ssl;

    private SocketClientConnectionInitializer socketClientConnectionInitializer;
    private SocketClientConnectionHandler socketClientConnectionHandler;
    private TcpOnClientConnectListener tcpOnClientConnectListener;
    private EventLoopGroup group;
    private Channel channel;
    private Channel channelToKill;
    private Queue<String> messageQueue;

    private final Object connectLock = new Object();
    public SocketClientConnector(String host, int port, boolean ssl) {
        this.host = host;
        this.port = port;
        this.ssl = ssl;
        group = new NioEventLoopGroup();
        messageQueue = new ConcurrentLinkedQueue<>();
        tcpOnClientConnectListener = this::onConnect;

        SslContext sslCtx = null;
        if(ssl){
            try {
                sslCtx = SslContextBuilder
                        .forClient()
                        .keyManager(
                                getClass().getResourceAsStream("/client.crt"),
                                getClass().getResourceAsStream("/client.key"),
                                "")
                        .build();
            }catch (Exception e){
                log.error("Failed to initialize SSL cert", e);
                try {
                    sslCtx = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
                } catch (SSLException ignored) {
                }
            }
        }

        socketClientConnectionHandler = new SocketClientConnectionHandler();
        socketClientConnectionHandler.setTcpOnClientConnectToHostListener(tcpOnClientConnectListener);

        socketClientConnectionInitializer = new SocketClientConnectionInitializer();
        socketClientConnectionInitializer.setSslCtx(sslCtx);
        socketClientConnectionInitializer.setServerAddress(host);
        socketClientConnectionInitializer.setServerPort(port);
        socketClientConnectionInitializer.setSocketConnectionHandler(socketClientConnectionHandler);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void connect() throws IOException {
        synchronized (connectLock){
            if(Objects.isNull(channel) || !channel.isActive()){
                if(Objects.nonNull(channelToKill) && channelToKill.isActive()){
                    channelToKill.close();
                }
                initConnection();
            }
        }
    }

    public void onConnect(Channel channel){
        this.channel = channel;
        for (int i = 0; i < messageQueue.size(); i++) {
            writeOnConnect(messageQueue.poll());
        }
    }

    private void initConnection() {
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(socketClientConnectionInitializer);

            // Start the connection attempt.
            channelToKill = b.connect(host, port).sync().channel();
        } catch (Exception ignored){
        }
    }

    public void writeToChannel(String message){
        if(Objects.nonNull(channel) && channel.isActive()){
            channel.writeAndFlush(message);
        }else{
            writeOnConnect(message);
            try {
                connect();
            } catch (IOException ignored) {
            }
            //TODO add onConnected listener
        }
    }

    public void writeOnConnect(String message){
        messageQueue.add(message);
    }

    public void shutdown(){
        if(Objects.nonNull(channel))
            channel.close();
        if(Objects.nonNull(channelToKill))
            channelToKill.close();
        try {
            group.shutdownGracefully().await();
        } catch (InterruptedException ignored) {
        }
    }
}
