/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.message.BasicAgentMessage;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class AgentRequestIdTrackingService extends Thread {
    public AgentRequestIdTrackingService() {
    }

    private long timeout = 5000;

    private final List<MessageReceiverItem> requests = new ArrayList<>();
    private final List<RequestListenerItem> requestHandlers = new ArrayList<>();
    private final Object requestLock = new Object();
    private final Object listenerLock = new Object();

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private IMultiAgentSystemConfiguration configController;

    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
        timeout = configController.getRequestRetentionTime();
    }

    public boolean addRequest(BasicAgentMessage message){
        long requestId = message.getRequestID();
        if(requestId==0)
            return true;

        boolean result;

        synchronized (requestLock){
            result = requests.stream().noneMatch(item->item.requestId == requestId);
            if(result){
                requests.add(new MessageReceiverItem(requestId, System.currentTimeMillis(), message));
            }
        }

        if(result){
            synchronized (listenerLock){
                List<RequestListenerItem> triggered = requestHandlers.stream().filter(item -> item.requestId==requestId).collect(Collectors.toList());
                for(RequestListenerItem item:triggered){
                    executorService.submit(() -> item.listener.onRequestId(message));
                }
                requestHandlers.removeAll(triggered);
            }
        }
        return result;
    }

    public void addRequestListener(long requestId, RequestReceiveListener listener){
        boolean triggered;
        synchronized (requestLock){
            Optional<MessageReceiverItem> setItem = requests.stream().filter(item -> item.requestId == requestId).findAny();
            triggered = setItem.isPresent();
            setItem.ifPresent(item -> executorService.submit(() -> listener.onRequestId(item.message)));
        }

        if(!triggered){
            synchronized (listenerLock){
                requestHandlers.add(new RequestListenerItem(requestId, System.currentTimeMillis(), listener));
            }
        }
    }

    public CompletableFuture<BasicAgentMessage> addRequestListenerBlockable(long requestId){
        CompletableFuture<BasicAgentMessage> completableFuture = new CompletableFuture<>();
        RequestReceiveListener futureListener = (message)-> completableFuture.complete(message);
        addRequestListener(requestId, futureListener);
        return completableFuture;
    }

    public BasicAgentMessage addRequestListenerBlock(long requestId) throws InterruptedException, ExecutionException, TimeoutException {
        return addRequestListenerBlockable(requestId).get(timeout, TimeUnit.MILLISECONDS);
    }

    public void cleanSet(long timestamp){
        synchronized (requestLock){
            requests.removeAll(requests.stream().filter(item -> item.timeReceived < timestamp).collect(Collectors.toList()));
        }
    }

    public void cleanListeners(long timestamp){
        synchronized (listenerLock){
            requestHandlers.removeAll(requestHandlers.stream().filter(item -> item.timeReceived < timestamp).collect(Collectors.toList()));
        }
    }

    public void run(){
        while(!isInterrupted()){
            cleanSet(System.currentTimeMillis() - timeout);
            cleanListeners(System.currentTimeMillis() - timeout);
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException ignored) {
            }
        }
    }

    public void shutdown(){
        this.interrupt();
    }

    private static class MessageReceiverItem {
        long requestId;
        long timeReceived;
        BasicAgentMessage message;

        public MessageReceiverItem(long requestId, long timeReceived, BasicAgentMessage message) {
            this.requestId = requestId;
            this.timeReceived = timeReceived;
            this.message = message;
        }
    }

    private static class RequestListenerItem {
        long requestId;
        long timeReceived;
        RequestReceiveListener listener;

        public RequestListenerItem(long requestId, long timeReceived, RequestReceiveListener listener) {
            this.requestId = requestId;
            this.timeReceived = timeReceived;
            this.listener = listener;
        }
    }

    public interface RequestReceiveListener{
        void onRequestId(BasicAgentMessage message);
    }
}
