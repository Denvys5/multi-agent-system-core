/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.service.network.ros;

import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import com.denvys5.multi_agent_system.model.agent.AgentCommunicationData;
import com.denvys5.multi_agent_system.config.dto.communication.AgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.config.dto.communication.RosAgentCommunicationInterfaceDataDto;
import com.denvys5.multi_agent_system.service.OnEventFutureService;
import com.denvys5.multi_agent_system.service.message_io.IMessageSerializationService;
import com.denvys5.multi_agent_system.service.network.AgentCommunicationInterface;
import com.denvys5.multi_agent_system.service.network.IMessageListener;
import com.denvys5.multi_agent_system.service.network.NetworkException;
import com.denvys5.multi_agent_system.service.network.NetworkOnConnectExecutor;
import com.denvys5.ros.controller.Publisher;
import com.denvys5.ros.controller.RosBridge;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
public class RosAgentCommunicationInterface extends Thread implements AgentCommunicationInterface {
    private RosBridge bridge;
    private List<Publisher> publisherCache;

    private IMultiAgentSystemConfiguration configController;

    private final OnEventFutureService networkOnConnectFutureService = new OnEventFutureService(1000);
    private AgentCommunicationData communicationData;
    private RosAgentCommunicationInterfaceDataDto rosCommunicationConfiguration;

    @Override
    public void setMessageSerializationService(IMessageSerializationService serializationService) {
    }

    @Override
    public void setConfigController(IMultiAgentSystemConfiguration configController) {
        this.configController = configController;
    }

    @Override
    public void setupConnectionProperties(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        rosCommunicationConfiguration = convertFromAbstract(communicationData.getCommsData());
    }

    public RosAgentCommunicationInterface(AgentCommunicationData communicationData) {
        this.communicationData = communicationData;
        rosCommunicationConfiguration = convertFromAbstract(communicationData.getCommsData());
    }

    @Override
    public AgentCommunicationData getCommunicationData() {
        return communicationData;
    }

    @Override
    public void thenConnected(NetworkOnConnectExecutor networkOnConnectExecutor) {
        CompletableFuture<Void> completableFuture = networkOnConnectFutureService.get();
        completableFuture.thenRun(() -> networkOnConnectExecutor.onConnect(this));
    }

    @Override
    public void init() {
        bridge = new RosBridge();
        publisherCache = new CopyOnWriteArrayList<>();
    }

    public void connector() {
        bridge.connect("ws://" + rosCommunicationConfiguration.getHost() + ":" + rosCommunicationConfiguration.getPort(), true);

    }

    private void recoverConnection(){
        if(!bridge.hasConnected()){
            try {
                connect();
            }catch (Exception e){
                if(configController.isDebug())
                    log.error(e.getMessage(), e);
            }
        }
    }


    public void run(){
        connector();

        while (!isInterrupted()){
            recoverConnection();
            try {
                Thread.sleep(configController.getReconnectDelay());
            } catch (InterruptedException ignored) {
            }
        }
    }

    @Override
    public void connect(){
        if(Objects.nonNull(bridge))
            this.start();
    }

    @Override
    public boolean isConnected() {
        return Objects.nonNull(bridge) && bridge.hasConnected();
    }

    @Override
    public void disconnect() {
        interrupt();
        bridge.closeConnection();
        publisherCache.clear();
    }

    public void subscribe(String topic, IMessageListener listener) throws NetworkException {
        try {
            bridge.subscribe(topic, "std_msgs/String",
                    (data, stringRep) -> {
                        try {
//                            TODO check this stuff, it is suspicious
                            listener.onMessageReceived(this.communicationData, data.getMsg().toString());
                        } catch (Exception ignored) {
                        }
                    }
            );
        }catch (Exception e){
            throw new NetworkException(e);
        }
    }

    @Override
    public void subscribe(AgentCommunicationInterfaceDataDto commsData, IMessageListener listener) throws NetworkException {
        RosAgentCommunicationInterfaceDataDto subscribeTo = convertFromAbstract(commsData);
        subscribe(subscribeTo.getInputTopic(), listener);
    }

    @Override
    public void subscribeToInput(IMessageListener listener) throws NetworkException {
        subscribe(rosCommunicationConfiguration.getInputTopic(), listener);
    }

    @Override
    public void subscribeToBroadcast(IMessageListener listener) throws NetworkException {
        subscribe(rosCommunicationConfiguration.getBroadcastTopic(), listener);
    }

    @Override
    public void unsubscribe() throws NetworkException {
        try {
            unsubscribe(rosCommunicationConfiguration.getBroadcastTopic());
            unsubscribe(rosCommunicationConfiguration.getInputTopic());
        }catch (Exception e){
            throw new NetworkException(e);
        }
    }

    public void unsubscribe(String topic) throws NetworkException {
        bridge.unsubscribe(topic);
    }

    public void publish(String topic, String payload) throws NetworkException {
        if(configController.isVerboseCommunication())
            log.info("Ros publish to" + topic + ": " + payload);
        Optional<Publisher> optionalPublisher = publisherCache.stream().filter(publisher -> publisher.getTopic().equals(topic)).findAny();
        if(optionalPublisher.isPresent()){
            optionalPublisher.get().publish(payload);
        }else{
            Publisher publisher = new Publisher(topic, "std_msgs/String", bridge);
            publisher.publish(payload);
            publisherCache.add(publisher);
        }
    }

    @Override
    public void sendMessageByCommsData(AgentCommunicationInterfaceDataDto commsData, String message) throws NetworkException {
        RosAgentCommunicationInterfaceDataDto publishTo = convertFromAbstract(commsData);
        publish(publishTo.getInputTopic(), message);
    }

    @Override
    public void broadcastMessageToAgents(String message) throws NetworkException {
        publish(rosCommunicationConfiguration.getBroadcastTopic(), message);
    }

    private RosAgentCommunicationInterfaceDataDto convertFromAbstract(AgentCommunicationInterfaceDataDto communicationData){
        return (RosAgentCommunicationInterfaceDataDto)communicationData;
    }
}
