/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.config.impl;

import com.denvys5.config.ConfigFactory;
import com.denvys5.multi_agent_system.config.IMultiAgentNetworkConfiguration;
import com.denvys5.multi_agent_system.model.agent.dto.MultiAgentNetworkConfigurationDto;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.ArrayList;

@Slf4j
@RequiredArgsConstructor
public class FileMultiAgentNetworkConfiguration implements IMultiAgentNetworkConfiguration {
    protected final Class<?> relativePathClass;
    protected final boolean devMode;
    @Getter
    protected MultiAgentNetworkConfigurationDto multiAgentNetworkConfiguration;

    @Setter
    @Getter
    protected String multiAgentNetworkConfigurationFilePath = "multi_agent_network.json";

    @Override
    public void init(){
        try {
            multiAgentNetworkConfiguration = ConfigFactory.getJsonObjectWithSwitchableRelativeFilePath(multiAgentNetworkConfigurationFilePath, MultiAgentNetworkConfigurationDto.class, relativePathClass, !devMode);
        } catch (FileNotFoundException | JsonIOException | JsonSyntaxException e) {
            log.error("Multi Agent Network Configuration is not initialized", e);
        }
        if(multiAgentNetworkConfiguration == null){
            multiAgentNetworkConfiguration = new MultiAgentNetworkConfigurationDto(new ArrayList<>());
            ConfigFactory.saveJsonObjectWithSwitchableRelativeFilePath(multiAgentNetworkConfigurationFilePath, multiAgentNetworkConfiguration, relativePathClass, !devMode);
        }
        log.info("Initialized MultiAgentNetworkConfiguration {}", multiAgentNetworkConfiguration);
    }

    @Override
    public void setMultiAgentNetworkConfiguration(MultiAgentNetworkConfigurationDto multiAgentNetworkConfiguration) {
        ConfigFactory.saveJsonObjectWithSwitchableRelativeFilePath(multiAgentNetworkConfigurationFilePath, multiAgentNetworkConfiguration, relativePathClass, !devMode);
        try {
            this.multiAgentNetworkConfiguration = ConfigFactory.getJsonObjectWithSwitchableRelativeFilePath(multiAgentNetworkConfigurationFilePath, MultiAgentNetworkConfigurationDto.class, relativePathClass, !devMode);
        } catch (FileNotFoundException | JsonIOException | JsonSyntaxException e) {
            log.error("Multi Agent Network Configuration is not initialized", e);
        }
        if(this.multiAgentNetworkConfiguration == null){
            this.multiAgentNetworkConfiguration = multiAgentNetworkConfiguration;
        }

    }
}
