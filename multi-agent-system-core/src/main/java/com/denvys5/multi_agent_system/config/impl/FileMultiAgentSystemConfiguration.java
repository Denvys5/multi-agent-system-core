/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.config.impl;

import com.denvys5.config.ConfigFactory;
import com.denvys5.config.EditableKeyValueConfig;
import com.denvys5.multi_agent_system.config.IMultiAgentSystemConfiguration;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@ToString
public class FileMultiAgentSystemConfiguration implements IMultiAgentSystemConfiguration {
    protected final Class<?> relativePathClass;
    protected final boolean devMode;

    @Getter
    protected boolean debug;
    @Getter
    protected boolean verboseCommunication;
    @Getter
    protected boolean secureCommunication;
    @Getter
    protected int reconnectDelay;
    @Getter
    protected String agentNameAlias;
    @Getter
    protected String agentUUID;
    @Getter
    protected long agentDiscoveryTimeOut;
    @Getter
    protected long requestRetentionTime;
    @Getter
    protected int pingTimeout;
    @Getter
    protected int minimalInitialTimeout;
    @Getter
    protected int maximumTimeout;

    @Setter
    @Getter
    protected String multiAgentSystemConfigFilePath = "multi_agent_network.cfg";

    @ToString.Exclude
    protected EditableKeyValueConfig config;

    @Override
    public void init(){
        config = ConfigFactory.getConfigWithSwitchableRelativePath(multiAgentSystemConfigFilePath, relativePathClass, !devMode);

        debug = config.addPropertyBoolean("debug", false);
        verboseCommunication = config.addPropertyBoolean("verboseCommunication", false);
        secureCommunication = config.addPropertyBoolean("secureCommunication", false);
        reconnectDelay = config.addPropertyInt("reconnectDelay", 10000);
        agentNameAlias = config.addPropertyString("deviceName", "DeviceTest");
        agentUUID = config.addPropertyString("deviceUUID", UUID.randomUUID().toString());
        agentDiscoveryTimeOut = config.addPropertyInt("agentDiscoveryTimeOut", 60000);
        requestRetentionTime = config.addPropertyInt("requestRetentionTime", 5000);
        pingTimeout = config.addPropertyInt("pingTimeout", 60000);
        minimalInitialTimeout = config.addPropertyInt("minimalInitialTimeout", 60000);
        maximumTimeout = config.addPropertyInt("maximumTimeout", 150000);

        log.info("Initialized MultiAgentSystemConfiguration {}", this);
    }
}
