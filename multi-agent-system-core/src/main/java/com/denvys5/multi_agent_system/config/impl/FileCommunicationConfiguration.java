/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.multi_agent_system.config.impl;

import com.denvys5.config.ConfigFactory;
import com.denvys5.multi_agent_system.config.ICommunicationConfiguration;
import com.denvys5.multi_agent_system.config.dto.AgentCommunicationConfiguration;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.util.ArrayList;

@Slf4j
@RequiredArgsConstructor
public class FileCommunicationConfiguration implements ICommunicationConfiguration {
    protected final Class<?> relativePathClass;
    protected final boolean devMode;

    @Getter
    protected AgentCommunicationConfiguration agentCommunicationConfiguration;

    @Setter
    @Getter
    protected String agentCommunicationConfigurationFilePath = "communication.json";

    @Override
    public void init(){
        try {
            agentCommunicationConfiguration = ConfigFactory.getJsonObjectWithSwitchableRelativeFilePath(agentCommunicationConfigurationFilePath, AgentCommunicationConfiguration.class, relativePathClass, !devMode);
        } catch (FileNotFoundException | JsonIOException | JsonSyntaxException e) {
            log.error("Network Configuration is not initialized", e);
        }
        if(agentCommunicationConfiguration == null){
            agentCommunicationConfiguration = new AgentCommunicationConfiguration(new ArrayList<>());
            ConfigFactory.saveJsonObjectWithSwitchableRelativeFilePath(agentCommunicationConfigurationFilePath, agentCommunicationConfiguration, relativePathClass, !devMode);
        }
        log.info("Initialized CommunicationConfiguration {}", agentCommunicationConfiguration);
    }

    @Override
    public void setAgentCommunicationConfiguration(AgentCommunicationConfiguration agentCommunicationConfiguration) {
        ConfigFactory.saveJsonObjectWithSwitchableRelativeFilePath(agentCommunicationConfigurationFilePath, agentCommunicationConfiguration, relativePathClass, !devMode);
        try {
            agentCommunicationConfiguration = ConfigFactory.getJsonObjectWithSwitchableRelativeFilePath(agentCommunicationConfigurationFilePath, AgentCommunicationConfiguration.class, relativePathClass, !devMode);
        } catch (FileNotFoundException | JsonIOException | JsonSyntaxException e) {
            log.error("Network Configuration is not initialized", e);
        }
        if(this.agentCommunicationConfiguration == null){
            this.agentCommunicationConfiguration = agentCommunicationConfiguration;
        }
    }
}
